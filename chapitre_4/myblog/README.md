# myblog

Serveur de blog avec routage, accès à une base de donnée SQLite et génération dynamieque de code HTML.

![](doc/home.png)

![](doc/write.png)

## avec Stack

```
sqlite3 myblog.db < myblog.sql
stack setup
stack build
stack exec myblog.out
```

puis aller à l'url <http://localhost:3000>


## avec Nix

```
sqlite3 myblog.db < myblog.sql
nix-shell --run "cabal run myblog.out"
```

puis aller à l'url <http://localhost:3000>

