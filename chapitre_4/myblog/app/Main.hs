{-# LANGUAGE OverloadedStrings #-}

import Control.Monad.Trans (liftIO)
import Web.Scotty (get, param, post, redirect, rescue, scotty, html)

import qualified Model
import qualified View

port = 3000

main = scotty port $ do

  get "/" $ do
    messages <- liftIO Model.selectMessages
    html $ View.homeRoute messages

  get "/write" $ html View.writeRoute

  post "/" $ do
    author <- param "author"
    title <- param "title"
    body <- param "body"
    liftIO $ Model.insertMessage $ Model.Message author title body
    redirect "/"

