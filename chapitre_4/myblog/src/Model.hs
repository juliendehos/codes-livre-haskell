{-# LANGUAGE OverloadedStrings #-}

module Model where

import qualified Data.Text as T
import qualified Database.SQLite.Simple as SQL
import Database.SQLite.Simple.FromRow (FromRow, fromRow, field)

-- Nom du fichier de base de données SQLite.
dbName = "myblog.db"

data Message = Message 
  { author :: T.Text
  , title  :: T.Text
  , body   :: T.Text
  }

instance FromRow Message where
  fromRow = Message <$> field <*> field <*> field

selectMessages :: IO [Message]
selectMessages = do
  conn <- SQL.open dbName
  res <- SQL.query_ conn "SELECT author,title,body FROM messages" 
         :: IO [Message]
  SQL.close conn
  return res

insertMessage :: Message -> IO ()
insertMessage msg = do
  conn <- SQL.open dbName
  let rq="INSERT INTO messages (author,title,body) VALUES (?,?,?)" 
  SQL.execute conn rq (author msg, title msg, body msg)
  SQL.close conn

