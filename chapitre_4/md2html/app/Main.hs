import HtmlWriter
import MdReader

main = do
  content <- getContents
  case mdReader content of
    Left  parseError  -> putStr $ formatError parseError
    Right parseResult -> putStr $ htmlWriter parseResult

