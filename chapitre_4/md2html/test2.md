# Markdown

Markdown est un langage de balisage léger créé par [John Gruber](https://fr.wikipedia.org/wiki/John_Gruber) en 2004. 

Son but est d'offrir une syntaxe facile à lire et à écrire. 

# Hypertext Markup Language

L’HyperText Markup Language, généralement abrégé HTML, est le langage de balisage conçu pour représenter les pages web. C’est un langage permettant d’écrire de l’hypertexte, d’où son nom.

