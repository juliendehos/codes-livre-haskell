module Document where

data TText = TNormal String TText
           | TUrl String String TText
           | TEndText

data TData = TTitle String TData
           | TBloc TText TData
           | TEndData

