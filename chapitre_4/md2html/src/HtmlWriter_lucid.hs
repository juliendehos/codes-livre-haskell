{-# LANGUAGE ExtendedDefaultRules #-}

module HtmlWriter (htmlWriter) where

import Document
import Lucid
import Data.Text.Lazy (unpack)
import Data.Text (pack)

htmlWriter :: TData -> String
htmlWriter = unpack . renderText . html_ . body_ . dataW

dataW :: TData -> Html ()
dataW (TTitle txt xs) = h1_ (toHtml txt) >> dataW xs
dataW (TBloc txts xs) = p_ (textW txts) >> dataW xs
dataW TEndData = return ()

textW :: TText -> Html ()
textW (TNormal txt xs) = toHtml txt >> textW xs
textW (TUrl txt url xs) = a_ [href_ (pack url)] (toHtml txt) >> textW xs
textW TEndText = return ()

