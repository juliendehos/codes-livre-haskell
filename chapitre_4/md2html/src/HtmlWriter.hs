module HtmlWriter (htmlWriter) where

import Document

htmlWriter :: TData -> String
htmlWriter d = "<!DOCTYPE html>\n<html>\n"
  ++ "  <head><meta charset=\"UTF-8\"/></head>\n"
  ++ "  <body>\n" ++ dataW d ++ "  </body>\n</html>"

dataW :: TData -> String
dataW (TTitle txt xs) = 
  "    <h1>" ++ txt ++ "</h1>\n" ++ dataW xs
dataW (TBloc txts xs) =
  "    <p>" ++ textW txts ++ "</p>\n" ++ dataW xs
dataW TEndData = ""

textW :: TText -> String
textW (TNormal txt xs) = txt ++ textW xs
textW (TUrl txt url xs) = 
  "<a href=\"" ++ url ++ "\">" ++ txt ++ "</a>" ++ textW xs
textW TEndText = ""

