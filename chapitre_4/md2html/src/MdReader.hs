module MdReader (mdReader, formatError) where

import Document

import Data.Void
import Text.Megaparsec
import Text.Megaparsec.Char
import Text.Megaparsec.Error

formatError :: ParseError Char Void -> String
formatError = parseErrorPretty

mdReader :: String -> Either (ParseError Char Void) TData
mdReader = parse dataR ""

dataR :: Parsec Void String TData
dataR = titleR
    <|> TBloc <$> textR <*> dataR
    <|> return TEndData

titleR :: Parsec Void String TData
titleR = TTitle <$> (string "# " >> some (notChar '\n')) 
                <*  some newline
                <*> dataR

textR :: Parsec Void String TText
textR = urlR 
    <|> TNormal <$> some (noneOf "\n[") <*> textR
    <|> TEndText <$ some newline

urlR :: Parsec Void String TText
urlR = TUrl <$> between (char '[') (char ']') (some $ notChar ']')
            <*> between (char '(') (char ')') (some $ notChar ')')
            <*> textR

