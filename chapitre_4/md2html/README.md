# md2html

Convertisseur Markdown (très simplifié) vers HTML. Utilise la bibliothèque de parseurs monadiques Megaparsec.

- exemple de fichier Markdown en entrée (`test.md`) :

```
# Markdown
Markdown est un langage de balisage léger créé par [John Gruber](https://fr.wikipedia.org/wiki/John_Gruber) en 2004. 
Son but est d'offrir une syntaxe facile à lire et à écrire. 
```

- exemple de fichier HTML produit :

```
<!DOCTYPE html>
<html>
  <head><meta charset="UTF-8"/></head>
  <body>
    <h1>Markdown</h1>
    <p>Markdown est un langage de balisage léger créé par <a href="https://fr.wikipedia.org/wiki/John_Gruber">John Gruber</a> en 2004. </p>
    <p>Son but est d'offrir une syntaxe facile à lire et à écrire. </p>
  </body>
</html>
```

![](doc/test.png)


## avec Stack

```
stack setup
stack build
stack exec md2html.out < test.md > test.html
```


## avec Nix

```
$ nix-build
$ ./result/bin/md2html.out < test.md  > test.html
```

