# madCustom

Idem que `mad` mais avec une implémentation perso de `State`.

Calcule une marche aléatoire discrète 2D dans une image (plus un pixel est
sombre, plus il a été visité). Illustre une utilisation de la monade `State`.

![](doc/mad.png)


## avec Stack

- compiler :

```
stack setup
stack build
```

- exécuter :

```
stack exec mad.out
```

voir l'image générée `mad.pnm`


## avec Nix

- compiler :

```
nix-build
```

- exécuter :

```
./result/bin/mad.out
```

voir l'image générée `mad.pnm`


