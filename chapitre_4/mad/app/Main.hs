import Image (creerImage, enregistrerPnm)
import MarcheAleatoire (mad)
import System.Random (getStdGen)

main :: IO ()
main = getStdGen >>= enregistrerPnm nomFichier . mad image nbPas 
  where nomFichier = "mad.pnm"
        image = creerImage 100 100
        nbPas = 10000

