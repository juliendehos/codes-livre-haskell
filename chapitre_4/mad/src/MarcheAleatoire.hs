module MarcheAleatoire 
    ( mad
    ) where

import Image (Image (..), Position, trouverVoisins, tracerChemin)
import Control.Monad (replicateM)
import Control.Monad.State.Lazy (State, get, put, evalState)
import System.Random (StdGen, randomR)

calculerPas :: Image -> State (Position, StdGen) Position
calculerPas image = do
  (pos0, gen0) <- get
  let voisins = trouverVoisins pos0 image
      (iVoisin, gen1) = randomR (0, length voisins - 1) gen0
      pos1 = voisins !! iVoisin
  put (pos1, gen1)
  return pos1

mad :: Image -> Int -> StdGen -> Image
mad image n gen = 
  let x0 = largeur_ image `div` 2
      y0 = hauteur_ image `div` 2
      etat0 = ((x0,y0), gen)
      marche = replicateM n (calculerPas image)
      positions = evalState marche etat0
  in tracerChemin image positions

