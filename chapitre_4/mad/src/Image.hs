module Image 
  ( Image (..)
  , creerImage
  , enregistrerPnm
  , Position
  , trouverVoisins
  , tracerChemin
  ) where

import qualified Data.Vector as V 
import System.IO (openFile, IOMode(..), hPrint, hPutStrLn, hClose)

type Position = (Int, Int)

data Image = Image 
  { largeur_ :: Int
  , hauteur_ :: Int
  , donnees_ :: V.Vector Int
  }

creerImage :: Int -> Int -> Image
creerImage l h = Image l h (V.replicate (l*h) 0)

enregistrerPnm :: String -> Image -> IO ()
enregistrerPnm nomFichier (Image l h d) = do
  fichier <- openFile nomFichier WriteMode
  hPutStrLn fichier "P2"
  hPutStrLn fichier $ show l ++ " " ++ show h
  hPutStrLn fichier "255"
  mapM_ (hPrint fichier . fQuantif) d
  hClose fichier
  where maxVal = V.maximum d
        fQuantif n = 255 - (n*255) `div` maxVal

trouverVoisins :: Position -> Image -> [Position]
trouverVoisins (x,y) (Image l h _) = 
  [ (x', y') | x'<-[x-1..x+1], y'<-[y-1..y+1], 
               x'>=0, x'<l, y'>=0, y'<h, x'/=x || y'/=y ]

positionVersIndice :: Image -> Position -> Int
positionVersIndice image (x,y) = y * largeur_ image + x

tracerChemin :: Image -> [Position] -> Image
tracerChemin image@(Image l h d) positions = Image l h d'
  where indices = map (positionVersIndice image) positions
        posVals = zip indices (repeat 1)
        d' = V.accum (+) d posVals

