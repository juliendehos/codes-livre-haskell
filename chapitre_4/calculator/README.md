# calculator

Calculatrice en notation préfixée gérant les opérateurs +, -, * et /.
Saisir une ligne vide pour terminer.

```
$ runghc calculator.hs 
Enter expression:
+ 1 2
3.0
Enter expression:
* 2 + 1 2
6.0
Enter expression:

$
```

