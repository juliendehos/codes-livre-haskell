import Control.Concurrent
import Control.Monad
import Network.Socket
import System.IO

-- Paramètre : 
-- port d'écoute.
echohsPort = 3000

-- Paramètre : 
-- nombre max de connexions clients.
echohsMaxConn = 5

main :: IO ()
main = withSocketsDo $ do
  -- Crée un socket d'écoute.
  myListenSocket <- socket AF_INET Stream defaultProtocol 
  bind myListenSocket $ SockAddrInet echohsPort iNADDR_ANY
  listen myListenSocket echohsMaxConn
  putStrLn $ "server running on port " ++ show echohsPort ++ "..."
  -- Lance l'écoute réseau.
  runServer myListenSocket

-- Lance le serveur d'écoute.
runServer :: Socket -> IO()
runServer theListenSocket = forever $ do
  -- Attend une connexion client et la gère dans un fork.
  (mySocket, _) <- accept theListenSocket
  forkIO $ do
    myHandle <- socketToHandle mySocket ReadWriteMode
    putStrLn "client connected"
    handleClient myHandle
    putStrLn "client disconnected"

-- Gère une connexion au client.
handleClient :: Handle -> IO ()
handleClient theHandle = do
  -- Lit un message du client.
  myMsg <- hGetLine theHandle 
  if null myMsg
  then 
    -- Termine si le message est vide.
    hClose theHandle
  else do
    -- Sinon répond au client avec le même message .
    putStrLn $ "echo: " ++ myMsg
    hPutStrLn theHandle myMsg
    -- Boucle récursivement pour un autre message.
    handleClient theHandle

