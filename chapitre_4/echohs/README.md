# echohs

Programmes client-serveur d'echo, avec des sockets.

## avec Stack

- compiler :

```
stack setup
stack build
```

- lancer le serveur :

```
$ stack exec echohs-server.out 
server running on port 3000...
client connected
echo: Bonjour, ici le client !
client disconnected
```

- lancer un client et entrer des messages (un message vide termine) :

```
$ stack exec echohs-client.out 

to server: 
Bonjour, ici le client !
from server: 
Bonjour, ici le client !

to server: 

$
```


## avec Nix

- compiler :

```
nix-build
```

- lancer le serveur :

```
$ ./result/bin/echohs-server.out 
server running on port 3000...
client connected
echo: Bonjour, ici le client !
client disconnected
```

- lancer un client et entrer des messages (un message vide termine) :

```
$ ./result/bin/echohs-client.out 

to server: 
Bonjour, ici le client !
from server: 
Bonjour, ici le client !

to server: 

$
```

