import Network.Socket
import System.IO

-- Paramètre : adresse IP du serveur.
echohsIp = tupleToHostAddress (127, 0, 0, 1)

-- Paramètre : port d'écoute du serveur.
echohsPort = 3000

main :: IO ()
main = withSocketsDo $ do

  -- Crée un socket vers le serveur.
  mySocket <- socket AF_INET Stream defaultProtocol 
  connect mySocket $ SockAddrInet echohsPort echohsIp
  myHandle <- socketToHandle mySocket ReadWriteMode

  -- Lance la communication avec le serveur.
  handleServer myHandle

-- Gère une connexion au serveur.
handleServer :: Handle -> IO ()
handleServer theHandle = do

  -- Récupère un message localement (saisie clavier).
  putStrLn "\nto server: "
  myLocalMsg <- getLine

  -- Envoie le message au serveur.
  hPutStrLn theHandle myLocalMsg
  if null myLocalMsg
  then 
    -- Termine si le message était vide.
    hClose theHandle
  else do 
    -- Sinon affiche la réponse du serveur.
    myServerMsg <- hGetLine theHandle 
    putStrLn "from server: "
    putStrLn myServerMsg
    -- Boucle récursivement pour un autre message.
    handleServer theHandle
