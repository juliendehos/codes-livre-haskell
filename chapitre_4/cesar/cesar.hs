import Data.Char (chr, isLower, ord)

decaler :: Int -> Char -> Char
decaler n c 
  | isLower c = chr $ ord 'a' + ((ord c + n - ord 'a') `mod` 26)
  | otherwise = c

chiffrerCesar :: Int -> String -> String
chiffrerCesar n = map (decaler n)

frequencesFr :: [Double]
frequencesFr =
  [9.41, 1.02, 2.64, 3.39, 15.86, 0.95, 1.04, 0.77, 8.41, 0.89, 
  0.01, 5.34, 3.24, 7.15, 5.14, 2.86, 1.06, 6.46, 7.90, 7.26, 
  6.24, 2.15, 0.01, 0.30, 0.24, 0.32]

compterOccurrences :: Char -> String -> Int
compterOccurrences c = foldl (\ n x -> if x==c then n+1 else n) 0

compterLettres :: String -> Int
compterLettres = length . filter isLower

calculerFrequences :: String -> [Double]
calculerFrequences s = 
  [fromIntegral (compterOccurrences c s) / n | c <- ['a'..'z']]
  where n = fromIntegral (compterLettres s)

calculerKhi2 :: [Double] -> [Double] -> Double
calculerKhi2 f' f = 
  sum [if x>0 then ((x'-x)**2) / x else 0 | (x',x) <- zip f' f]

casserCesar :: String -> [Double] -> Int
casserCesar txt f = snd $ minimum [(khi2 i, i) | i<-[0..25]]
  where khi2 i' = calculerKhi2 (freqs i') f
        freqs i' = calculerFrequences $ chiffrerCesar i' txt

main = do
  putStrLn $ "texte chiffré : " ++ txt
  putStrLn $ "clé : " ++ show cle
  putStrLn $ "texte déchiffré : " ++ txt'
  where txt = "sh cpjavpyl lza iypsshual, s'ljolj lza tha"
        cle = casserCesar txt frequencesFr
        txt' = chiffrerCesar cle txt

