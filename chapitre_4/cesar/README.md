# Cesar

Chiffrement de César et attaque par force brute.

## programme de test

```console
$ runghc cesar.hs 
texte chiffré : sh cpjavpyl lza iypsshual, s'ljolj lza tha
cle : 19
texte déchiffré : la victoire est brillante, l'echec est mat
```

## avec ghci

```ghci
$ ghci
GHCi, version 8.2.2: http://www.haskell.org/ghc/  :? for help
Prelude> :load cesar.hs
[1 of 1] Compiling Main             ( cesar.hs, interpreted )
Ok, one module loaded.

*Main> msg_chiffre = "sh cpjavpyl lza iypsshual, s'ljolj lza tha"

*Main> casserCesar msg_chiffre frequencesFr
19

*Main> chiffrerCesar 19 msg_chiffre 
"la victoire est brillante, l'echec est mat"
```

