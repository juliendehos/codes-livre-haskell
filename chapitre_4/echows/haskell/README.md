# echows (Haskell)

Programme client-serveur d'écho, avec des websockets.

![](echows_hs.png)

## avec Stack

- compiler :

```
stack setup
stack build
```

- lancer le serveur :

```
stack exec echows-server
```

- lancer le client :

```
stack exec echows-client
```

## avec Nix

- compiler :

```
nix-build
```

- lancer le serveur :

```
./result/bin/echows-server
```

- lancer le client :

```
./result/bin/echows-client
```

