{ pkgs ? import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/17.09.tar.gz") {} }:
let 
  drv = pkgs.haskellPackages.callCabal2nix "echows" ./. {};
in
if pkgs.lib.inNixShell then drv.env else drv

