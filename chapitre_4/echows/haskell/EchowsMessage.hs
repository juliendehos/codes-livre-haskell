module EchowsMessage where

import qualified Data.Text as T
import qualified Network.WebSockets as WS

decodeMsg :: WS.DataMessage -> T.Text
decodeMsg (WS.Text t) = WS.fromLazyByteString t
decodeMsg _ = T.pack ""

