import           Control.Monad (forever)
import qualified Data.Text as T
import           EchowsMessage (decodeMsg)
import qualified Network.WebSockets as WS

port = 8080

main = do
  putStrLn $ "running server on port " ++ show port ++ "..." 
  WS.runServer "0.0.0.0" port serverApp

serverApp :: WS.PendingConnection -> IO ()
serverApp pc = WS.acceptRequest pc >>= forever . gererConnexion 

gererConnexion :: WS.Connection -> IO ()
gererConnexion c = do
  msg <- WS.receiveDataMessage c
  putStrLn $ T.unpack $ decodeMsg msg
  WS.sendDataMessage c msg

