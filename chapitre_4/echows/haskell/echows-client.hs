import           Control.Monad (forever)
import qualified Data.Text as T
import           EchowsMessage (decodeMsg)
import qualified Network.WebSockets as WS

url = "localhost" 
port = 8080
path = ""

main = WS.runClient url port path clientApp

clientApp :: WS.Connection -> IO () 
clientApp conn = forever $ do
  putStrLn "to server: "
  msgToSrv <- getLine
  WS.sendTextData conn $ T.pack msgToSrv
  msgFromSrv <- WS.receiveDataMessage conn
  putStrLn "from server: "
  putStrLn $ T.unpack $ decodeMsg msgFromSrv

