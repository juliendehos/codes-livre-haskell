const WebSocket = require('ws');

const my_port = 8080;

const my_ws = new WebSocket.Server({ port: my_port });

my_ws.on('connection', function (ws) {
  ws.on('message', function(msg) {
    console.log('echo: %s', msg);
    ws.send(msg);
  });
});

console.log(`running server on port ${my_port}...`);

