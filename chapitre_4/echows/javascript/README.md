# echows (JavaScript : Node.js + page web)

Programme client-serveur d'écho, avec des websockets.

![](echows_js.png)

## avec Node.js et le paquet ws déjà installés

- lancer le serveur :

```
node echows-server.js
```

- ouvrir la page `echows-client.html` dans un navigateur


## avec NixOS

- installer Node.js et configurer le projet :

```
nix-env -iA nixos.nodejs nixos.nodePackages.node2nix
node2nix -6 -d -i package.json 
```

- lancer le serveur :

```
nix-shell -A shell --run "node echows-server.js"
```

- ouvrir la page `echows-client.html` dans un navigateur

