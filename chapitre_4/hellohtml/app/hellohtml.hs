import System.Environment (getArgs, getProgName)
import qualified Data.Text.Lazy as L
import qualified Data.Text.Lazy.IO as LIO
import Text.HelloHtml (Page (..), calculerPage)

main :: IO ()
main = do
  args <- getArgs
  if length args /= 2
  then do
    progName <- getProgName
    putStrLn $ "usage: " ++ progName ++ " <titre> <corps>"
  else do
    let titre = L.pack $ head args
        corps = L.pack (args !! 1)
    LIO.putStrLn $ calculerPage (Page titre corps)

