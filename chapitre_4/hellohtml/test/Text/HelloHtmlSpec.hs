{-# LANGUAGE OverloadedStrings #-}

module Text.HelloHtmlSpec (main, spec) where

import qualified Data.Text.Lazy as L
import Test.Hspec
import Test.QuickCheck
import Text.HelloHtml

main :: IO ()
main = hspec spec

-- Définit une suite de tests pour le module HelloHtml.
spec :: Spec
spec = do

  describe "tests unitaires" $ 
    it "helloworld" $ 
      calculerPage (Page "Hello" "World !")
      `shouldBe` 
      "<!DOCTYPE HTML><html><body><h1>Hello</h1><p>World !</p></body></html>"

  describe "tests QuickCheck" $ 
    it "prop_html" $ property prop_html

-- Propriétés à vérifier avec QuickCheck.
prop_html :: PageData -> Bool
prop_html (PageData t c) = calculerPage page == resultat
  where page = Page (L.pack t) (L.pack c)
        resultat = L.concat ["<!DOCTYPE HTML><html><body><h1>", 
            L.pack t, "</h1><p>", L.pack c, "</p></body></html>"]

-- Générateur de données d'entrée pour QuickCheck.
genSafeString :: Gen String
genSafeString = listOf $ elements (['a'..'z'] ++ ['A'..'Z'] 
                                ++ ['0'..'9'] ++ " ,;.?!()")

data PageData = PageData String String deriving Show

instance Arbitrary PageData where
  arbitrary = PageData <$> genSafeString <*> genSafeString

