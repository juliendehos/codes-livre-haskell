# hellohtml

Projet Haskell classique avec bibliothèque + programme + test + doc.
Génère une page HTML à partir d'un titre et d'un texte donnés en paramètres.

![](hellohtml.png)

## avec Stack

- compilation :

```
$ stack setup
$ stack build
```

- execution :

```
$ stack exec hellohtml.out "mon titre" "mon texte"
<!DOCTYPE HTML><html><body><h1>mon titre</h1><p>mon texte</p></body></html>
```

- test :

```
$ stack test
hellohtml-0.1: test (suite: spec)

Text.HelloHtml
  tests unitaires
    helloworld
  tests QuickCheck
    prop_html

Finished in 0.0042 seconds
2 examples, 0 failures

hellohtml-0.1: Test suite spec passed
```

- documentation :

```
$ stack haddock
```

puis ouvrir la page `.stack-work/install/x86_64-linux/lts-11.9/8.2.2/doc/all/index.html` dans un navigateur web




## avec Nix

- exécution directe :

```
$ nix-build

$ ./result/bin/hellohtml.out "mon titre" "mon texte"
<!DOCTYPE HTML><html><body><h1>mon titre</h1><p>mon texte</p></body></html>
```

- exécution dans un nix-shell :

```
$ nix-shell

$ cabal run "mon titre" "mon texte"
...
```

- tests (dans un nix-shell) :

```
$ cabal test
...
```

- documentation (dans un nix-shell) :

```
$ cabal haddock
```

puis ouvrir la page `dist/doc/html/hellohtml/index.html` dans un navigateur web


