-- |
-- Module: Text.HelloHtml
-- Copyright: (c) 2018 Julien Dehos
-- License: BSD3
--
-- Un générateur de pages HTML simples, contenant 
-- un titre et un corps (sous forme de lazy `Text`). 
--
-- > import Text.HelloHtml
-- > pagehtml = calculerPage (Page "titre" "corps")
module Text.HelloHtml 
  ( -- * Types de données
    Page (..)
    -- * Fonctions
  , calculerPage
  ) where

import Data.Text.Lazy (Text)
import Lucid

-- | Page avec un titre et un corps. 
data Page = Page 
  { titrePage :: Text  -- ^ titre de la page
  , corpsPage :: Text  -- ^ corps de la page
  }

-- | Calcule le `Text` du code HTML de la page
calculerPage 
  :: Page  -- ^ page à transformer en HTML
  -> Text  -- ^ code HTML correspondant
calculerPage page = renderText $ doctypehtml_ $
  body_ $ do
    h1_ $ toHtml $ titrePage page
    p_  $ toHtml $ corpsPage page

