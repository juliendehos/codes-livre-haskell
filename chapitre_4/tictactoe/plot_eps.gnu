set terminal postscript eps size 4,3 enhanced color 
set style data linespoints
set grid xtics ytics
set xlabel 'nbSims'
set ylabel 'nbWins'
set key box right center
set out 'out.eps'
plot 'out.dat' using 1:4 lw 3 title 'Monte-Carlo', \
     'out.dat' using 1:3 lw 3 title "Random", \
     'out.dat' using 1:2 lw 3 title 'Draws'

