# tictactoe

Implémente un jeu de tic-tac-toe...

![](doc/tictactoe_board.png)

... et confronte quelques bots (random et monte-carlo)

![](doc/benchmarks.png)


## avec Stack

```
stack setup
stack build
stack exec tictactoe.out > out.dat
./result/bin/tictactoe.out > out.dat
gnuplot plot.gnu
```

puis ouvrir l'image `out.png` générée

ou pour jouer interactivement :

```
$ stack ghci
Configuring GHCi with the following packages: tictactoe
GHCi, version 8.2.2: http://www.haskell.org/ghc/  :? for help
...
Ok, one module loaded.
Loaded GHCi configuration from /run/user/1000/ghci9478/ghci-script

*Main> :load tictactoe
[1 of 1] Compiling Main             ( tictactoe.hs, interpreted )
Ok, one module loaded.

*Main> jeu1 = jouerCoup jeuInitial 4

*Main> jeu1
Tictactoe {grille = [0,0,0,0,1,0,0,0,0], nbCoupsPossibles = 8, joueurCourant = 2, joueurSuivant = 1}

...
```


## avec Nix

```
nix-build
./result/bin/tictactoe.out > out.dat
gnuplot plot.gnu
```

puis ouvrir l'image `out.png` générée

ou pour jouer interactivement :

```
$ nix-shell --run ghci
Configuring GHCi with the following packages: tictactoe
GHCi, version 8.2.2: http://www.haskell.org/ghc/  :? for help
...
Ok, one module loaded.
Loaded GHCi configuration from /run/user/1000/ghci9478/ghci-script

*Main> :load tictactoe
[1 of 1] Compiling Main             ( tictactoe.hs, interpreted )
Ok, one module loaded.

*Main> jeu1 = jouerCoup jeuInitial 4

*Main> jeu1
Tictactoe {grille = [0,0,0,0,1,0,0,0,0], nbCoupsPossibles = 8, joueurCourant = 2, joueurSuivant = 1}

...
```


