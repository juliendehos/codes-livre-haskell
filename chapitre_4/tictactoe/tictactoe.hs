import           Control.Monad (foldM_)
import qualified Data.Vector as V 
import           System.Random (getStdGen, StdGen, randomR)
import           Text.Printf (printf)
(!) = (V.!)

data Tictactoe = Tictactoe 
  { grille           :: V.Vector Int
  , nbCoupsPossibles :: Int
  , joueurCourant    :: Int
  , joueurSuivant    :: Int
  } deriving (Show)

jeuInitial = Tictactoe (V.replicate 9 0) 9 1 2

trouverIndice :: V.Vector Int -> Int -> Int
trouverIndice grille iCoup = fst $ coupsLibres!iCoup
  where indices = V.fromList [0..8]
        zipIndiceGrille = V.zip indices grille
        coupsLibres = V.filter (\(_,g) -> g==0) zipIndiceGrille

jouerCoup :: Tictactoe -> Int -> Tictactoe
jouerCoup jeu iCoup = 
  if grilleCourante!iGrille /= 0 then jeu else jeu'
  where grilleCourante = grille jeu
        iGrille = trouverIndice grilleCourante iCoup
        joueur = joueurCourant jeu
        coups = V.fromList [(iGrille, joueur)]
        grille' = V.update grilleCourante coups
        nbCoupsPossibles' = nbCoupsPossibles jeu - 1
        jeu' = Tictactoe grille' nbCoupsPossibles' 
                         (joueurSuivant jeu) joueur

calculerFin :: Tictactoe -> (Bool, Int)
calculerFin jeu
  | g!0 /= 0 && g!0 == g!1 && g!1 == g!2 = (True, g!0)  -- ligne 1
  | g!3 /= 0 && g!3 == g!4 && g!4 == g!5 = (True, g!3)  -- ligne 2
  | g!6 /= 0 && g!6 == g!7 && g!7 == g!8 = (True, g!6)  -- ligne 3
  | g!0 /= 0 && g!0 == g!3 && g!3 == g!6 = (True, g!0)  -- col 1
  | g!1 /= 0 && g!1 == g!4 && g!4 == g!7 = (True, g!1)  -- col 2
  | g!2 /= 0 && g!2 == g!5 && g!5 == g!8 = (True, g!2)  -- col 3
  | g!0 /= 0 && g!0 == g!4 && g!4 == g!8 = (True, g!0)  -- diag 1
  | g!2 /= 0 && g!2 == g!4 && g!4 == g!6 = (True, g!2)  -- diag 2
  | nbCoupsPossibles jeu == 0 = (True, 0)               -- égalité
  | otherwise = (False, 0)                              -- en cours
  where g = grille jeu 

type Bot = Tictactoe -> StdGen -> (Int, StdGen) 

botRandom :: Bot
botRandom jeu = randomR (0, nbCoupsPossibles jeu - 1)

jouerJeu :: Tictactoe -> Bot -> Bot -> StdGen 
         -> (Int, Tictactoe, StdGen)
jouerJeu jeu bot1 bot2 rng = 
  if estFini then (vainqueur, jeu', rng') else continue
  where bot = if joueurCourant jeu == 1 then bot1 else bot2
        (iCoup, rng') = bot jeu rng
        jeu' = jouerCoup jeu iCoup
        (estFini, vainqueur) = calculerFin jeu'
        continue = jouerJeu jeu' bot1 bot2 rng'

evalCoup :: Int -> Int -> Bot
evalCoup iCoup nbSims jeu rng = 
  if nbCoupsPossibles jeu == 1 
  then (0, rng) 
  else evalCoupAux nbSims 0 rng jeu iCoup

evalCoupAux 0 nbVicts rng _ _ = (nbVicts, rng)
evalCoupAux nbSims nbVicts rng jeu iCoup = 
  evalCoupAux (nbSims-1) nbVicts' rng' jeu iCoup
  where jeu1 = jouerCoup jeu iCoup
        joueur = joueurCourant jeu
        (vainq, _, rng') = jouerJeu jeu1 botRandom botRandom rng
        nbVicts' = if vainq == joueur then nbVicts+1 else nbVicts

botMc :: Int -> Bot
botMc maxSims jeu = botMcAux 0 0 0 nbSimsParCoup jeu
  where nbSimsParCoup = max 1 $ div maxSims (nbCoupsPossibles jeu)

botMcAux :: Int -> Int -> Int -> Int -> Bot
botMcAux iCoup iBest vBest nbSimsParCoup jeu rng =
  if iCoup == nbCoupsPossibles jeu
  then (iBest, rng)
  else botMcAux (iCoup + 1) iBest' vBest' nbSimsParCoup jeu rng'
  where (vCoup, rng') = evalCoup iCoup nbSimsParCoup jeu rng
        (vBest', iBest') = max (vCoup, iCoup) (vBest, iBest)

jouerJeux :: Tictactoe -> Bot -> Bot -> Int -> StdGen 
          -> (Int, Int, Int, StdGen)
jouerJeux jeu bot1 bot2 nbJeux rng = 
  jouerJeuxAux jeu bot1 bot2 nbJeux (0, 0, 0, rng)

jouerJeuxAux jeu bot1 bot2 nbJeux res = 
  if nbJeux == 0
  then res
  else jouerJeuxAux jeu bot1 bot2 (nbJeux-1) res'
  where (egals, victs1, victs2, rng) = res
        (vainqueur, _, rng') = jouerJeu jeu bot1 bot2 rng
        res' = case vainqueur of 
                  0 -> (egals+1, victs1, victs2, rng')
                  1 -> (egals, victs1+1, victs2, rng')
                  2 -> (egals, victs1, victs2+1, rng')
                  _ -> error "unknown winner"

calculerXp :: Int -> StdGen -> Int -> IO StdGen
calculerXp nbJeux rng maxSims = do
  let (egalsA1B2, victsA1, victsB2, rng')  = 
         jouerJeux jeuInitial botRandom (botMc maxSims) nbJeux rng
      (egalsB1A2, victsB1, victsA2, rng'') = 
         jouerJeux jeuInitial (botMc maxSims) botRandom nbJeux rng'
  printf "%d %d %d %d %d\n" maxSims (egalsA1B2+egalsB1A2) 
     (victsA1+victsA2) (victsB1+victsB2) (nbJeux*2)
  return rng''

main = do
  putStrLn "nbSims draws winsA winsB nbGames"
  rng <- getStdGen
  foldM_ (calculerXp 500) rng [2^i | i<-[1..8]]

