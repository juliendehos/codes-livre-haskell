# spirale

Lit une matrice dans un fichier CSV et affiche son parcours en spirale (en partant du coin en haut à gauche).

Par exemple, avec le fichier `matrice.csv` : 

```
$ cat matrice.csv 
a;b;c;d;e
f;g;h;i;j
k;l;m;n;o
p;q;r;s;t
u;v;w;x;y
z;aa;ab;ac;ad
```

on doit avoir :

```
a b c d e j o t y ad ac ab aa z u p k f g h i n s x w v q l m r
```

## avec Stack

```
$ stack setup
$ stack build
$ stack exec spirale.out matrice.csv
```


## avec Nix

```
$ nix-build
$ ./result/bin/spirale.out matrice.csv 
```

