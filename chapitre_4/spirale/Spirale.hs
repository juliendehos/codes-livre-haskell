module Spirale (spirale) where

import Data.List (transpose)

operations :: [([[a]]->[a], [[a]]->[[a]])]
operations = cycle [ (head,tail), 
                     (last,init), 
                     (reverse.last,init), 
                     (reverse.head,tail) ]

parcourir :: [([[a]]->[a], [[a]]->[[a]])] -> [[a]] -> [a]
parcourir _ [] = []
parcourir (op:ops) mat = mat0 ++ parcourir ops (transpose mat1)
  where (fct0, fct1) = op
        mat0 = fct0 mat
        mat1 = fct1 mat

spirale :: [[a]] -> [a]
spirale = parcourir operations
