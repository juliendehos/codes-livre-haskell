import Data.List.Split (splitOn)
import Spirale
import System.Environment (getArgs, getProgName)

main = do
  args <- getArgs
  if length args /= 1
  then do
    nomProg <- getProgName
    putStrLn $ "usage: " ++ nomProg ++ " <filename>"
  else do
    fichier <- readFile (head args)
    let matrice = map (splitOn ";") $ lines fichier
    putStrLn $ unwords $ spirale matrice
