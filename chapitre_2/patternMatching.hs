-- Utilise un filtrage par motif pour définir une fonction.
formaterNombre :: (Eq a, Num a) => a -> String
formaterNombre 0 = "zero"
formaterNombre 1 = "un"
formaterNombre _ = "plusieurs"   -- Cas par défaut.

-- Filtrage par motif sur un tuple.
myFst :: (a, b) -> a
myFst (x, _) = x

-- Filtrage par motif sur un tuple, avec alias (@).
seuillerTuple :: (Int, a) -> (Int, a)
seuillerTuple t@(x, y) = if x < 42 then (42, y) else t

-- Filtrage par motif pour définir des variables locales.
seuillerTuple' :: (Int, a) -> (Int, a)
seuillerTuple' t = if x < 42 then (42, y) else t
  where (x, y) = t

-- Filtrage par motif sur une liste.
myTail :: [a] -> [a]
myTail [] = []      -- Liste vide.
myTail (_:xs) = xs  -- Liste composée d'une tête et d'une queue.

-- Filtrage par motif sur une liste.
deuxElements :: [a] -> Bool
deuxElements [_, _] = True  -- Liste à deux éléments.
deuxElements _ = False      -- Autres listes.

main :: IO ()
main = do
  print $ formaterNombre 0
  print $ formaterNombre 1
  print $ formaterNombre 2
  print $ myFst (1, 2)
  print $ seuillerTuple (7, "toto")
  print $ seuillerTuple' (7, "toto")
  print $ myTail [1..4]
  print $ deuxElements [42, 3]
  print $ deuxElements [1..4]

