{- Ceci est un 
   commentaire 
   multi-ligne.

   {- Ceci est un commentaire imbriqué. -}
-}

x = 1   -- Ceci est un commentaire de fin de ligne.

-- Les commentaires sont souvent utilisés en en-tête de fonction.
main = print x
