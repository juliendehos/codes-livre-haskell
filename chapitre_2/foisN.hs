-- La fonction foisN retourne une fonction.
foisN :: Int -> (Int -> Int)
foisN n = \ x -> n * x

res21fois2 :: Int
res21fois2 = (foisN 21) 2  -- (foisN 21) retourne une fonction
                           -- que l'on peut donc évaluer pour 2.

-- Définition équivalente de foisN.
foisN' :: Int -> (Int -> Int)
foisN' = \ n -> (\ x -> n * x)

-- Autre définition équivalente de foisN.
foisN'' :: Int -> Int -> Int    -- Pas besoin de parenthèse.
foisN'' n x = n * x    -- Syntaxe avec plusieurs paramètres.

-- Définition équivalente de res21fois2.
res21fois2'' :: Int
res21fois2'' = foisN'' 21 2    -- Pas besoin de parenthèse.
                               -- Aussi possible : foisN 21 2.

main = do
  print $ foisN 21 2
  print $ foisN' 21 2
  print $ foisN'' 21 2
