-- Définit un nouvel opérateur binaire.
(|+|) :: Num a => a -> a -> a
(|+|) x y = abs x + abs y  -- Autre syntaxe possible :
                           -- x |+| y = abs x + abs y.
b :: Int
b = 2 |+| (-3)      -- Évaluation, syntaxe opérateur.

b' :: Int
b' = (|+|) 2 (-3)   -- Évaluation, syntaxe fonction.

-- Definit une fonction 
-- à 2 paramètres.
normeCarree :: Num a => a -> a -> a
normeCarree x y = x*x + y*y

n :: Double
n = normeCarree 3 4     -- Évaluation, syntaxe fonction.

n' :: Double
n' = 3 `normeCarree` 4  -- Évaluation, syntaxe opérateur.

main = do
  print n
  print n'
  print b
  print b'

