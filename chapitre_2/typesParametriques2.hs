repeter :: [a] -> [a]
repeter xs = xs ++ xs

main = do
  print $ repeter [1, 2]
  print $ repeter ["Haskell", "OCaml"]

