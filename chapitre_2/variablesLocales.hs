-- Syntaxe avec le mot-clé "where".
x = y + z       -- Définit x globalement.
  where y = 12  -- Définit y localement.
        z = 30  -- Définit z localement.

-- Syntaxe avec "let in".
u = let v = 10  -- Définit v localement.
        w = 27  -- Définit w localement.
    in v + w    -- Définit u globalement.

-- Dans l'espace global, x et u sont définis et utilisables 
-- mais y, z, v et w n'existent pas.

main = do
  print x
  print u
