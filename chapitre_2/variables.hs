str = "hello"   -- Définit une variable str de valeur "hello".
nb = 21*2       -- Définit une variable d'après une expression.

-- Programme principal.
main = do
  print str     -- Affiche la valeur de la variable str.
  print nb      -- Affiche la valeur de la variable nb.
