-- Fonction récursive qui calcule la taille d'une liste.
taille :: [a] -> Int
taille [] = 0
taille (_:xs) = 1 + taille xs

-- Fonction qui regroupe les éléments d'une liste par paires.
-- Par exemple "genPaires [1..7]" retourne "[(1,2),(3,4),(5,6)]".
genPaires :: [a] -> [(a, a)]
genPaires (x0:x1:xs) = (x0, x1) : genPaires xs
genPaires _ = []

main = do
  print $ taille [1..4]
  print $ genPaires [1..8]
  print $ genPaires [1..7]

