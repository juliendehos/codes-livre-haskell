x = y
  where y = 1  -- Ok.

u = v + w
  where v = 2
        w = 3  -- Ok.

main = do
  print x
  print u      -- Ok.
