-- Définition explicite d'une fonction f : pour chaque valeur 
-- du domaine, on définit la valeur correspondante du codomaine.
f 0 = 0
f 1 = 2
f 2 = 4

-- Évalue f pour la valeur 2, ce qui produit la valeur 4 car
-- d'après la définition : f 2 = 4.
n = f 2
main = print n

