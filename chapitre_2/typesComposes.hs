listeNoms :: [[Char]]
listeNoms = ["Haskell Curry", "Alan Turing"]

famillesLangages :: [(String, [String])]
famillesLangages = [("Lisp", ["Common Lisp", "Scheme"]),
                    ("ML", ["Haskell", "OCaml", "F#"])]

main = do
  print listeNoms
  print famillesLangages

