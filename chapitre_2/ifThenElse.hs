x :: Int
x = 3

-- Utilise un if-then-else pour définir une variable.
y :: Int
y = if x < 42 then 42 else x

-- Utilise un if-then-else pour définir une fonction.
seuiller :: (Ord a, Num a) => a -> a
seuiller n = if n < 42 then 42 else n

-- Utilise un if-then-else dans une expression.
z :: Int
z = 2 * (if x < 42 then 42 else x)

main :: IO ()
main = do
  print y 
  print $ seuiller 7
  print z

