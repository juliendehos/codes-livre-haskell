-- Liste (infinie) des termes de la suite de Fibonacci.
fibonacci :: [Int]
fibonacci = 0 : 1 : suivant fibonacci
  where suivant (x0:x1:xs) = (x0+x1) : suivant (x1:xs)

main = do
  print $ fibonacci !! 9     -- 9e terme : f(9).
  print $ take 10 fibonacci  -- 10 premiers termes : f(0) à f(9).

