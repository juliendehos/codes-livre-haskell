-- Définition de fonction classique.
f1 :: Int -> Int
f1 x = x * 2

-- Définition équivalente, avec une lambda.
f2 :: Int -> Int
f2 = (\ x -> x * 2)

-- Applique une fonction (lambda) sur les 
-- éléments d'une liste.
xs :: [Int]
xs = map (\ x -> x * 2) [1..5]

main = do
  print $ f1 21
  print $ f2 21
  print xs
  
