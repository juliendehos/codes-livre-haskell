f1 x = succ $ negate $ abs x  -- Définition pour un point x.

f1' x = succ (negate (abs x)) -- Notation équivalente.

f2 = succ . negate . abs      -- Definition par composition
                              -- (notation point-free).

ajouter x y = x + y        -- Fonction à 2 paramètres.

ajouter37 y = ajouter 37 y -- Définition pour un point y du domaine.

ajouter42 = ajouter 42     -- Définition par évaluation partielle
                           -- (notation point-free).

main = do
  print $ ajouter37 1
  print $ ajouter42 1
  print $ f1 (-2)
  print $ f2 (-2)

