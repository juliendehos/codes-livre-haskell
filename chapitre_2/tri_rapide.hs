tri_rapide [] = []
tri_rapide (pivot:liste) = (tri_rapide liste_inf) ++ [pivot] ++ (tri_rapide liste_sup)
    where liste_inf = filter (< pivot) liste
          liste_sup = filter (>= pivot) liste

main = print $ tri_rapide [3, 4, 1, 37, 42, 8, 13]
