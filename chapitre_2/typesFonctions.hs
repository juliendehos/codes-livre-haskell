pairImpair :: Int -> String
pairImpair n = if even n then "pair" else "impair"

roundDiv :: Double -> Double -> Int
roundDiv x y = round (x / y)

main = do
  print $ pairImpair 7
  print $ roundDiv 20.4 2.1

