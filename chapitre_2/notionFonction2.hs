-- Définition d'une fonction f par une expression : on définit 
-- le calcul à réaliser en fonction d'un paramètre x.
f x = x*2

-- Évalue f sur la valeur 2, ce qui produit la valeur 4 car
-- d'après la définition f x = x*2 donc f 2 = 2*2 donc f 2 = 4.
n = f 2
main = print n

