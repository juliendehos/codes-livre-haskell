-- Définit un module qui exporte uniquement plus2.
module Math2 (plus2)
where

plus1 :: Num a => a -> a
plus1 = (+1)

plus2 :: Num a => a -> a
plus2 = plus1 . plus1

