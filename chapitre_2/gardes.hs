-- Définit une fonction en utilisant des gardes.
negNulPos :: (Ord a, Num a) => a -> String
negNulPos x
  | x == 0    = "nul"
  | x < 0     = "negatif"
  | otherwise = "positif"

-- Utilisation combinée du filtrage par motif et des gardes.
unElementPositif :: (Ord a, Num a) => [a] -> String
unElementPositif [] = "liste vide"
unElementPositif [x]
  | x > 0     = "oui"
  | x == 0    = "on peut dire cela"
  | otherwise = "non"
unElementPositif _ = "trop d'elements"

main = do
  print $ negNulPos 0
  print $ negNulPos (-1)
  print $ negNulPos 2
  print $ unElementPositif []
  print $ unElementPositif [42]
  print $ unElementPositif [0]
  print $ unElementPositif [1..4]

