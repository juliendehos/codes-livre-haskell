import Math1
import Math2

main = do
  print $ Math1.plus1 41  -- Ok.
  print $ Math1.plus2 40  -- Ok.
  print $ Math2.plus1 41  -- Erreur (Math2 n'exporte pas plus1).
  print $ Math2.plus2 40  -- Ok.

