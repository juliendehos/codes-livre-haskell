import Data.Char (toUpper)

main = do
  str <- getContents  -- Récupère les données de l'entrée 
                      -- standard dans une variable "str".

  putStrLn (map toUpper str)  -- Met le texte "str" en 
                              -- majuscule et l'affiche.

