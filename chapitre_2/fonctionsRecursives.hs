-- Fonction récursive calculant la factorielle.
fact :: Int -> Int
fact n = if n == 1 then 1                 -- Cas terminal.
                   else n * fact (n - 1)  -- Appel récursif.

-- Fonction récursive terminale, où acc doit être initialisé à 1.
factTerm :: Int -> Int -> Int
factTerm acc n = if n == 1 then acc else factTerm (acc*n) (n-1)

-- Fonction récursive terminale sans paramètre à initialiser.
factTerm' :: Int -> Int
factTerm' = fAux 1
  where fAux acc n = if n == 1 then acc else fAux (acc*n) (n-1)

main = do
  print $ fact 1
  print $ fact 2
  print $ fact 3
  print $ fact 4
  print $ fact 5
  print $ factTerm 1 5
  print $ factTerm' 5

