main = do
  putStrLn "Entrez votre nom : "  -- Affiche un texte à l'écran.

  nom <- getLine  -- Saisit un texte au clavier 
                  -- et le met dans une variable "nom".

  putStrLn ("Bonjour " ++ nom ++ " !")

