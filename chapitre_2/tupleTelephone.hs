-- Définit un tuple à deux éléments.
samu :: (String, Integer)
samu = ("SAMU", 15)

-- Récupère les valeurs du tuple, par pattern matching.
(nom, num) = samu

-- Récupère la seconde valeur uniquement.
(_, num') = samu

-- Récupère la seconde valeur avec la fonction snd 
-- (snd fonctionne uniquement sur les tuples à deux éléments).
num'' = snd samu

main = do
  print nom
  print num
  print num'
  print num''

