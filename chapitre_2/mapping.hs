-- Fonction récursive qui 
-- applique la fonction "even" sur 
-- chaque élément d'une liste.
--
estPair :: (Integral a) => [a] -> [Bool]
estPair [] = []
estPair (x:xs) = (even x) : estPair xs

-- Autre implémentation de "estPair" en utilisant "map".
estPair' :: (Integral a) => [a] -> [Bool]
estPair' = map even

-- Exemple d'implémentation (récursive non-terminale) du "mapping".
myMap :: (a -> b) -> [a] -> [b]
myMap _ [] = []
myMap f (x:xs) = (f x) : myMap f xs

main = do
  print $ estPair [1..4]
  print $ estPair' [1..4]
  print $ myMap even [1..4]

