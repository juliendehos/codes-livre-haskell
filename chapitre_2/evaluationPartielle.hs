ajouter :: Int -> Int -> Int  -- Fonction à deux paramètres.
ajouter x y = x + y

ajouter42 :: Int -> Int       -- Fonction à un paramètre.
ajouter42 = ajouter 42  -- Évaluation partielle de ajouter
                        -- (où x est fixé à 42).

-- Définit une fonction à 4 paramètres.
f1 :: Num a => a -> a -> a -> a -> a
f1 x y z w = (x+y) * (z+w)

-- Définit une fonction à 2 paramètres.
f2 :: Num a => a -> a -> a
f2 = f1 2 3  -- Évaluation partielle de f1 
             -- (où x est fixé à 2 et y à 3).

main = do
  print $ ajouter42 1
  print $ f2 2 3

