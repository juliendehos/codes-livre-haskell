tripler :: Int -> Int
tripler x = x * 3

tripler' :: (Num a) => a -> a    -- Fonction polymorphe avec 
tripler' x = x * 3               -- contrainte de type.

main = do
  print $ tripler' 14      -- Utilisation sur des entiers.
  print $ tripler' 14.0    -- Utilisation sur des réels.

