-- Variable de type Int.
n :: Int
n = 21

-- Fonction de type Int -> Int.
doubler :: Int -> Int
doubler x = x * 2

-- Type du programme principal.
main :: IO ()
main = print $ doubler n

