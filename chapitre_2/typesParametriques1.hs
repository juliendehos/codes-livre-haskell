repeterInt :: [Int] -> [Int]
repeterInt xs = xs ++ xs

repeterString :: [String] -> [String]
repeterString xs = xs ++ xs

main = do
  print $ repeterInt [1, 2]
  print $ repeterString ["Haskell", "OCaml"]

