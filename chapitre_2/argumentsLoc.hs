import System.Environment (getArgs)
main = do
  args <- getArgs  -- Récupère la liste des arguments.
  print args       -- Affiche la liste récupérées.
