x :: Int
x = 42

-- Utilise un case-of pour définir une variable.
str :: String
str = case x of 0 -> "zero"
                1 -> "un"
                _ -> "plusieurs"  -- Cas par défaut.

-- Utilise un case-of pour définir une fonction.
formaterNombre :: (Eq a, Num a) => a -> String
formaterNombre n = case n of 0 -> "zero"
                             1 -> "un"
                             _ -> "plusieurs"

main :: IO ()
main = do
  print x
  print str
  print $ formaterNombre 0
  print $ formaterNombre 1
  print $ formaterNombre 2

