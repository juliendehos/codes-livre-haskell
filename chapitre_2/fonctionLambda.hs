-- Définition de fonction classique.
f1 :: Num a => a -> a
f1 x = x * 2

-- Définition équivalente, avec une lambda.
f2 :: Num a => a -> a
f2 = (\ x -> x * 2)

-- Applique une fonction (lambda) sur les éléments d'une liste.
-- Résultat : [2,4,6,8,10].
xs :: [Int]
xs = map (\ x -> x * 2) [1..5]

main = do
  print $ f1 21
  print $ f2 21
  print xs
  
