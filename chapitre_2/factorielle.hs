-- Définit une fonction qui calcule n!.
factorielle 0 = 1
factorielle n = n * factorielle (n - 1)

-- Évalue la fonction sur le nombre 5.
main = print $ factorielle 5
