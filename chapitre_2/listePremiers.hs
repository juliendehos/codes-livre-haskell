-- Liste (infinie) des nombres premiers 
-- (méthode du crible d'Ératosthène).
premiers = crible [2..]
    where crible (p:xs) = p : crible [x | x <- xs, x `mod` p /= 0]

main = do
  print $ take 20 premiers
