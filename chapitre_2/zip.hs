-- Trouve les indices d'un élément e dans une liste xs.
-- Par exemple, trouverIndices 3 [1,3,2,3] retourne [1,3].
trouverIndices e xs = foldr fAux [] xs'
  where fAux (i,x) acc = if x==e then i:acc else acc
        xs' = zip [0..] xs

-- Exemple d'implémentation possible de zip.
myZip :: [a] -> [b] -> [(a,b)]
myZip [] _ = []
myZip _ [] = []
myZip (x:xs) (y:ys) = (x,y) : myZip xs ys

-- Utilisation de zipWith pour calculer la suite de Fibonacci.
fibonacci :: [Int]
fibonacci = 0 : 1 : zipWith (+) fibonacci (tail fibonacci)

main = do
  print $ trouverIndices 3 [1,3,2,3] 
  print $ trouverIndices 't' "toto"
  print $ myZip [1..] ["haskell", "ocaml", "lisp"]
  print $ take 10 fibonacci

