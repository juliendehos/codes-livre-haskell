-- Fonction récursive qui calcule la somme d'une liste.
somme :: (Num a) => [a] -> a
somme [] = 0
somme (x:xs) = x + somme xs

-- Idem mais avec une réduction.
somme' :: (Num a) => [a] -> a
somme' = foldl (+) 0

-- Exemple d'implémentation de "foldl".
myFoldl :: (a -> b -> a) -> a -> [b] -> a
myFoldl _ acc [] = acc
myFoldl f acc (x:xs) = myFoldl f (f acc x) xs

-- Calcule la taille d'une liste avec une réduction.
taille :: [a] -> Int
taille = foldl (\ acc _ -> acc+1) 0

-- Renverse une liste avec une réduction.
renverser :: [a] -> [a]
renverser = foldl (flip (:)) []

main = do
  print $ somme [1..4]
  print $ somme' [1..4]
  print $ myFoldl (+) 0 [1..4]
  print $ taille [1..4]
  print $ renverser [1..4]

