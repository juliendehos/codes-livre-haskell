-- Définit un module qui exporte tout (plus1 et plus2).
module Math1 where

plus1 :: Num a => a -> a
plus1 = (+1)

plus2 :: Num a => a -> a
plus2 = plus1 . plus1

