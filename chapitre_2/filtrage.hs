-- Fonction récursive qui récupère les éléments pairs d'une liste.
garderPairs :: (Integral a) => [a] -> [a]
garderPairs [] = []
garderPairs (x:xs) = if even x then x : xs' else xs'
  where xs' = garderPairs xs

-- Autre implémentation de "garderPairs" en utilisant "filter".
garderPairs' :: (Integral a) => [a] -> [a]
garderPairs' = filter even

-- Exemple d'implémentation (récursive non-terminale) du "filter".
myFilter :: (a -> Bool) -> [a] -> [a]
myFilter _ [] = []
myFilter p (x:xs) = if p x then x : xs' else xs'
  where xs' = myFilter p xs

main = do
  print $ garderPairs [1..4]
  print $ garderPairs' [1..4]
  print $ myFilter even [1..4]


