x = y
where y = 3    -- Erreur.

u = v + w
  where v = 2
      w = 3    -- Erreur.

main = do
  print x
    print u    -- Erreur.
