plus05 :: Int -> Double
plus05 x = (fromIntegral x) + 0.5

positif :: Double -> Bool
positif x = x > 0

positifApresPlus05 :: Int -> Bool
positifApresPlus05 = positif . plus05  -- Compose 2 fonctions.

comp3 :: Int -> String
comp3 = show . positif . plus05  -- Composition de 3 fonctions.

main = putStrLn $ comp3 (-2)

