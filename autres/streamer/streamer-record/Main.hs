{-# LANGUAGE OverloadedStrings #-}

import           Control.Concurrent (threadDelay)
import           Control.Monad (forever)
import qualified GI.Gdk as Gdk
import qualified GI.Gtk as Gtk
import           GI.GdkPixbuf.Objects.Pixbuf
import           GI.GObject.Objects.Object
import qualified Network.WebSockets as WS
import           System.Environment (getArgs)

main :: IO ()
main = do
    window <- initGtk
    args <- getArgs
    case args of
        [ip, portStr] -> WS.runClient ip (read portStr) "" (clientApp window)
        _ -> putStrLn "usage: <ip> <port>"

initGtk :: IO Gdk.Window
initGtk = do
    _ <- Gtk.init Nothing
    Just screen <- Gdk.screenGetDefault
    Gdk.screenGetRootWindow screen

clientApp :: Gdk.Window -> WS.ClientApp ()
clientApp window conn = forever $ do
    mPixBuf <- Gdk.pixbufGetFromWindow window 0 0 640 480
    case mPixBuf of
        Nothing -> putStr "warning: pixbufGetFromWindow failed"
        Just pixBuf -> do
            img <- pixbufSaveToBufferv pixBuf "jpeg" ["quality"] ["50"]
            WS.sendBinaryData conn img
            objectUnref pixBuf
    threadDelay 500000

