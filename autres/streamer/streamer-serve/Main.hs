{-# LANGUAGE OverloadedStrings #-}

import           Control.Monad (forever)
import qualified Data.ByteString.Lazy as BS
import           Data.IORef (readIORef, atomicWriteIORef, IORef, newIORef)
import           Data.Maybe (fromMaybe)
import           Network.Wai (Application)
import           Network.Wai.Handler.WebSockets (websocketsOr)
import qualified Network.WebSockets as WS
import           System.Environment (lookupEnv)
import qualified Web.Scotty as SC

main :: IO ()
main = do
    img <- BS.readFile "static/welcome.jpg"
    imgRef <- newIORef img
    port <- read . fromMaybe "3000" <$> lookupEnv "PORT"
    putStrLn $ "listening port " ++ show port ++ "..."
    SC.scotty port $ do
        SC.middleware (wsApp imgRef)
        httpApp imgRef

wsApp :: IORef BS.ByteString -> Application -> Application
wsApp imgRef = websocketsOr WS.defaultConnectionOptions (wsHandle imgRef)

wsHandle :: IORef BS.ByteString -> WS.PendingConnection -> IO ()
wsHandle imgRef pc = do
    conn <- WS.acceptRequest pc
    forever (WS.receiveData conn >>= atomicWriteIORef imgRef)

httpApp :: IORef BS.ByteString -> SC.ScottyM ()
httpApp imgRef = do
    SC.get "/" $ SC.file "static/index.html"
    SC.get "/img" $ do
        SC.addHeader "Content-Type" "image/jpeg"
        SC.raw =<< SC.liftAndCatchIO (readIORef imgRef)

