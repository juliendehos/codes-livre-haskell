CREATE TABLE messages (
  id INTEGER PRIMARY KEY,
  author TEXT,
  title TEXT,
  body TEXT
);

INSERT INTO messages VALUES( 
  0, 
  "Wikipedia", 
  "Programmation fonctionnelle", 
  "La programmation fonctionnelle est un paradigme de 
   programmation de type déclaratif qui considère le calcul 
   en tant qu'évaluation de fonctions mathématiques."
);

INSERT INTO messages VALUES( 
  1, 
  "Wikipedia", 
  "Haskell", 
  "Haskell est un langage de programmation fonctionnel. Il est 
   fondé sur le lambda-calcul et la logique combinatoire. Son 
   nom vient du mathématicien et logicien Haskell Brooks Curry."
);

-- create database with: sqlite3 simpleblog.db < simpleblog.sql

