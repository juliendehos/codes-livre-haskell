# Simpleblog (Haskell)

## Description

Simpleblog est un serveur HTTP qui permet de lire des messages de blog dans
une base de données et d'en fournir des pages HTML. 

Il s'agit d'un exemple d'application inspiré du livre : [La programmation
fonctionnelle - Introduction et application en Haskell à l’usage de
l’étudiant et du
développeur](https://www.editions-ellipses.fr/programmation-fonctionnelle-introduction-applications-haskell-lusage-letudiant-developpeur-p-13083.html).

![simpleblog](doc/screen-haskell.gif)

## Dépendances

- pour la base de données : [Sqlite](https://www.sqlite.org)

- pour le code Haskell : [Stack](https://docs.haskellstack.org) ou
[Nix](https://nixos.org/nix/)

## Utilisation

- générer la base de données Sqlite :

    ```text
    sqlite3 simpleblog.db < simpleblog.sql
    ```

- compiler et lancer le serveur :

  - avec Stack :

    ```text
    stack build --exec simpleblog
    ```

  - avec Nix :

    ```text
    nix-shell --run "cabal run"
    ```

- ouvrir un navigateur et aller à l'adresse `http://localhost:3000` 
