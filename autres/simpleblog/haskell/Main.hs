{-# LANGUAGE OverloadedStrings #-}

import           Control.Monad (forM_)
import           Control.Monad.Trans (liftIO)
import qualified Data.Text.Lazy as L
import           Database.SQLite.Simple (query_, withConnection)
import           Database.SQLite.Simple.FromRow (FromRow, fromRow, field)
import           Lucid
import           Web.Scotty (get, scotty, html)

-- Model

data Message = Message 
    { _author :: L.Text
    , _title  :: L.Text
    , _body   :: L.Text
    }

instance FromRow Message where
    fromRow = Message <$> field <*> field <*> field

selectMessages :: IO [Message]
selectMessages = withConnection "simpleblog.db" req
    where req c = query_ c "SELECT author,title,body FROM messages"

-- View

aboutView :: L.Text
aboutView = renderText $ do
    doctype_
    html_ $ body_ $ do
        h1_ "À propos"
        p_ "Ceci est un blog sur Haskell, codé en Haskell."
        p_ $ a_ [href_ "/"] "Accueil..."

homeView :: [Message] -> L.Text
homeView messages = renderText $ do
    doctype_
    html_ $ body_ $ do
        h1_ "Mon blog sur Haskell"
        forM_ messages $ \ m -> p_ $ div_ $ do
            strong_ $ toHtml $ _title m
            toHtml $ L.concat [ " par ", _author m ]
            div_ $ toHtml $ _body m
        p_ $ a_ [href_ "/about"] "À propos..."

-- main

main = scotty 3000 $ do
    get "/about" $ html aboutView
    get "/" $ liftIO selectMessages >>= html . homeView
