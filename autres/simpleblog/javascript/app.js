"use strict";

// Model

const db = require("better-sqlite3")("simpleblog.db");

function selectMessages() {
    return db.prepare("SELECT author,title,body FROM messages").all();
}

// View

const pug = require('pug');

const aboutView = pug.compile(`
doctype html
html
    body
    h1 À propos
    p Ceci est un blog sur JavaScript, codé en JavaScript.
    a(href="/") Accueil...
`);

const homeFunc = pug.compile(`
doctype html
html
    body
    h1 Mon blog sur JavaScript
    each msg in messages
        p 
            strong #{msg.title} 
            .
                par #{msg.author}
            br
            .
                #{msg.body}
    a(href="/about") À propos...
`);
const homeView = (messages) => homeFunc(messages);

// main
    
const express = require("express");
const app = express();

app.get("/about", function (request, response) {
    const html = aboutView();
    response.send(html);
});

app.get("/", function (request, response) {
    const messages = selectMessages();
    const html = homeView({messages});
    response.send(html);
});

app.listen(3000, function () {
    console.log(`Listening on port 3000...`);
});
