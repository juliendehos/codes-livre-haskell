# Simpleblog (JavaScript)

## Description

Simpleblog est un serveur HTTP qui permet de lire des messages de blog dans
une base de données et d'en fournir des pages HTML. 

Il s'agit d'un exemple d'application inspiré du livre : [La programmation
fonctionnelle - Introduction et application en Haskell à l’usage de
l’étudiant et du
développeur](https://www.editions-ellipses.fr/programmation-fonctionnelle-introduction-applications-haskell-lusage-letudiant-developpeur-p-13083.html).

![simpleblog](doc/screen-javascript.gif)

## Dépendances

- pour la base de données : [Sqlite](https://www.sqlite.org)

- pour le code JavaScript : [Node.js](https://nodejs.org) ou
[Nix](https://nixos.org/nix/)

## Utilisation

- générer la base de données et lancer le serveur :

  - avec Node.js :

    ```text
    make node
    ```

  - avec Nix :

    ```text
    make nix
    ```

- ouvrir un navigateur et aller à l'adresse `http://localhost:3000` 
