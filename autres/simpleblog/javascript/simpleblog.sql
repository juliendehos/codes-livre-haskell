CREATE TABLE messages (
  id INTEGER PRIMARY KEY,
  author TEXT,
  title TEXT,
  body TEXT
);

INSERT INTO messages VALUES( 
  0, 
  "Wikipedia", 
  "Programmation orientée prototype", 
  "La programmation orientée prototype est une forme de programmation orientée
   objet sans classe, fondée sur la notion de prototype. Un prototype est un
   objet à partir duquel on crée de nouveaux objets."
);

INSERT INTO messages VALUES( 
  1, 
  "Wikipedia", 
  "JavaScript", 
  "JavaScript est un langage de programmation de scripts principalement employé
   dans les pages web interactives mais aussi pour les serveurs avec
   l'utilisation (par exemple) de Node.js."
);

-- create database with: sqlite3 simpleblog.db < simpleblog.sql

