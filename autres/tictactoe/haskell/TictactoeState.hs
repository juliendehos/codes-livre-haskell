{-# LANGUAGE TupleSections #-}
import Control.Monad.State
import qualified Data.Vector as V 
import System.Random (getStdGen, StdGen, randomR)

type Joueur = Char
type Coup = Int

data Tictactoe = Tictactoe 
    { grille           :: V.Vector Joueur
    , coupsPossibles   :: V.Vector Coup
    , joueurCourant    :: Joueur
    , joueurSuivant    :: Joueur
    } deriving (Show)

jeuInitial :: Tictactoe
jeuInitial = Tictactoe (V.replicate 9 '.') (V.fromList [0..8]) 'X' 'O'

jouerCoup :: Tictactoe -> Coup -> Tictactoe
jouerCoup jeu coup = 
    if coup `elem` coupsPossibles jeu then jeu' else error "coup invalide"
    where grille' = grille jeu V.// [(coup, joueurCourant jeu)]
          coups' = V.filter (/= coup) (coupsPossibles jeu) 
          jeu' = Tictactoe grille' coups' (joueurSuivant jeu) (joueurCourant jeu)

calculerFin :: Tictactoe -> (Bool, Joueur)
calculerFin jeu
    | g!0 /= '.' && g!0 == g!1 && g!1 == g!2 = (True, g!0)  -- ligne 1
    | g!3 /= '.' && g!3 == g!4 && g!4 == g!5 = (True, g!3)  -- ligne 2
    | g!6 /= '.' && g!6 == g!7 && g!7 == g!8 = (True, g!6)  -- ligne 3
    | g!0 /= '.' && g!0 == g!3 && g!3 == g!6 = (True, g!0)  -- col 1
    | g!1 /= '.' && g!1 == g!4 && g!4 == g!7 = (True, g!1)  -- col 2
    | g!2 /= '.' && g!2 == g!5 && g!5 == g!8 = (True, g!2)  -- col 3
    | g!0 /= '.' && g!0 == g!4 && g!4 == g!8 = (True, g!0)  -- diag 1
    | g!2 /= '.' && g!2 == g!4 && g!4 == g!6 = (True, g!2)  -- diag 2
    | null (coupsPossibles jeu) = (True, '.')               -- égalité
    | otherwise = (False, '.')                              -- en cours
    where g = grille jeu 
          (!) = (V.!)

type Bot = Tictactoe -> State StdGen Coup

botRandom :: Bot
botRandom jeu = do
    rng <- get
    let nbCoups = length $ coupsPossibles jeu
        (iCoup, rng') = randomR (0, nbCoups - 1) rng
        coup = coupsPossibles jeu V.! iCoup
    put rng'
    return coup

jouerJeu :: Tictactoe -> Bot -> Bot -> State StdGen (Joueur, Tictactoe)
jouerJeu jeu botX botO = do
    let bot = if joueurCourant jeu == 'X' then botX else botO
    coup <- bot jeu
    let jeu' = jouerCoup jeu coup
        (estFini, vainqueur) = calculerFin jeu'
    if estFini then return (vainqueur, jeu') else jouerJeu jeu' botX botO

evalCoup :: Int -> Tictactoe -> Coup -> State StdGen (Int, Coup)
evalCoup nbSims jeu coup = do
    let joueur = joueurCourant jeu
        jeu1 = jouerCoup jeu coup
    (, coup) . sum <$> replicateM nbSims (do
                            (vainq, _) <- jouerJeu jeu1 botRandom botRandom
                            return $ if vainq == joueur then 1 else 0)

botMc :: Int -> Bot
botMc maxSims jeu = do
    let coups = coupsPossibles jeu
        nbCoups = length coups
        nbSimsParCoup = max 1 $ div maxSims nbCoups
    snd . maximum <$> mapM (evalCoup nbSimsParCoup jeu) coups

main :: IO ()
main = do
    (winner, jeu) <- evalState (jouerJeu jeuInitial botRandom (botMc 100)) <$> getStdGen
    print jeu
    putStrLn $ "winner: " ++ show winner

