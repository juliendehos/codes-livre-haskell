#include <algorithm>
#include <array>
#include <iostream>
#include <random>
#include <stdexcept>

using namespace std;
using Player = char;
using Move = int;

class Tictactoe {
    private:
        array<Player, 9> _board;
        int _nbFreeMoves;
        int _currentPlayer;
        int _nextPlayer;
        bool _finished;
        int _winner;

    public:
        Tictactoe() : 
            _board({'.', '.', '.', '.', '.', '.', '.', '.', '.'}),
            _nbFreeMoves(9),
            _currentPlayer('X'),
            _nextPlayer('O'),
            _finished(false),
            _winner('.') {
            }

        Player getCurrentPlayer() const { return _currentPlayer; }

        int getNbFreeMoves() const { return _nbFreeMoves; }

        Player getWinner() const { return _winner; }

        bool getFinished() const { return _finished; }

        void playIndex(Move moveIndex) {
            if (moveIndex<0 or moveIndex >= _nbFreeMoves)
                throw logic_error("invalid index");
            // find index
            int nbFree = moveIndex;
            Move k = 0;
            while (k<9) {
                if (_board[k] == '.') {
                    nbFree--;
                    if (nbFree <= 0)
                        break;
                }
                ++k;
            }
            // update board
            if (k == 9) throw logic_error("invalid index");
            _nbFreeMoves--;
            _board[k] = _currentPlayer;
            swap(_currentPlayer, _nextPlayer);
            // check winner
            auto & b = _board;
            _finished = true;
            _winner = '.';
            if (b[0] != '.' and b[0] == b[1] and b[1] == b[2])
                _winner = b[0];
            else if (b[3] != '.' and b[3] == b[4] and b[4] == b[5])
                _winner = b[3];
            else if (b[6] != '.' and b[6] == b[7] and b[7] == b[8])
                _winner = b[6];
            else if (b[0] != '.' and b[0] == b[3] and b[3] == b[6])
                _winner = b[0];
            else if (b[1] != '.' and b[1] == b[4] and b[4] == b[7])
                _winner = b[1];
            else if (b[2] != '.' and b[2] == b[5] and b[5] == b[8])
                _winner = b[2];
            else if (b[0] != '.' and b[0] == b[4] and b[4] == b[8])
                _winner = b[0];
            else if (b[2] != '.' and b[2] == b[4] and b[4] == b[6])
                _winner = b[2];
            else if (_nbFreeMoves != 0)
                _finished = false;
        }

        friend ostream & operator<<(ostream & os, const Tictactoe & game);
};

ostream & operator<<(ostream & os, const Tictactoe & game) {
    for (int i=0; i<3; i++) {
        for (int j=0; j<3; j++) {
            os << game._board[i*3+j] << ' ';
        }
        os << endl;
    }
    os << "current: " << char(game._currentPlayer) << endl;
    os << "winner: " << char(game._winner) << endl;
    os << "finished: " << game._finished << endl;
    return os;
}

class Rng {
    private:
        mt19937_64 _engine;
        uniform_real_distribution<double> _distribution;

    public:
        Rng() : 
            _engine(random_device{}()),
            _distribution(0, 1) {
            }

        Rng(const Rng &) = delete;

        int operator()(int nMax) {
            return min(nMax-1, int(nMax*_distribution(_engine)));
        }
};

class Bot {
    public:
        virtual Move genmove(const Tictactoe & game) = 0;
};

class RandomBot : public Bot {
    private:
        Rng _rng;

    public:
        Move genmove(const Tictactoe & game) override {
            int nbMoves = game.getNbFreeMoves();
            return _rng(nbMoves);
        }
};

void runGame(Tictactoe & game, Bot & botX, Bot & botO) {
    while (not game.getFinished()) {
        Bot & bot = game.getCurrentPlayer() == 'X' ? botX : botO;
        Move moveIndex = bot.genmove(game);
        game.playIndex(moveIndex); 
    } 
}

class McBot : public Bot {
    private:
        int _maxSims;
        RandomBot _randomBot;

    public:
        explicit McBot(int maxSims) : _maxSims(maxSims) {}

        Move genmove(const Tictactoe & game) override {
            Player player = game.getCurrentPlayer();
            int nbMoves = game.getNbFreeMoves();
            int nbMoveSims = max(1, int(_maxSims / nbMoves));
            Move bestIndex = 0;
            int bestWins = 0;
            for (Move moveIndex=0; moveIndex<nbMoves; ++moveIndex) {
                int moveWins = 0;
                Tictactoe game1 = game;
                game1.playIndex(moveIndex);
                for (int sims=0; sims<nbMoveSims; sims++) {
                    Tictactoe gameN = game1;
                    runGame(gameN, _randomBot, _randomBot);
                    if (gameN.getWinner() == player) 
                        moveWins++;
                }
                if (moveWins > bestWins) {
                    bestWins = moveWins;
                    bestIndex = moveIndex;
                }
            }
            return bestIndex;
        }
};

int main() {
    try {
        Tictactoe game;
        RandomBot botX;
        McBot botO(100);
        runGame(game, botX, botO);
        cout << game << endl;
    }
    catch(const logic_error & e) {
        cerr << "error: " << e.what() << endl;
    }
    return 0;
}

