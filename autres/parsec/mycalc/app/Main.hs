import Control.Monad
import System.Environment
import System.IO

import Mycalc.Codegen
import Mycalc.Syntax
import qualified Mycalc.Parser.Monadic
import qualified Mycalc.Parser.Parsec
import qualified Mycalc.Parser.Readp
import qualified Mycalc.Parser.Recursive

main :: IO ()
main = do
    args <- getArgs
    case args of
        ["monadic"]    -> putStrLn "\nmonadic"    >> run Mycalc.Parser.Monadic.parseExpr
        ["parsec"]     -> putStrLn "\nparsec"     >> run Mycalc.Parser.Parsec.parseExpr
        ["readp"] -> putStrLn "\nreadp" >> run Mycalc.Parser.Readp.parseExpr
        ["recursive"]  -> putStrLn "\nrecursive"  >> run Mycalc.Parser.Recursive.parseExpr
        _ -> putStrLn "usage: <monadic|parsec|readp|recursive>"

run :: (String -> Either String Expr) -> IO ()
run parseExpr = do
    putStr "\n> "
    hFlush stdout
    input <- getLine
    when (input /= "") $ do
        case parseExpr input of
            Left err -> putStrLn $ "error: " ++ err
            Right expr -> do
                print expr
                putStrLn $ toLisp expr
                print $ eval expr
        run parseExpr

