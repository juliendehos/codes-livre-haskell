module Mycalc.Parser.Readp where

import Control.Applicative
import Data.Char
import Text.ParserCombinators.ReadP

import Mycalc.Syntax

-------------------------------------------------------------------------------
-- main types & functions
-------------------------------------------------------------------------------

parseExpr :: String -> Either String Expr
parseExpr s0 = case readP_to_S additiveP s0 of
    [] -> Left "no parsing"
    xs -> Right $ fst $ last xs

-------------------------------------------------------------------------------
-- parsing primitives
-------------------------------------------------------------------------------

digitP :: ReadP Char
digitP = satisfy isDigit

digitsP :: ReadP String
digitsP = many1 digitP

natP :: ReadP Int
natP = read <$> digitsP

-------------------------------------------------------------------------------
-- grammar
-------------------------------------------------------------------------------

decimalP :: ReadP Expr
decimalP = ExprVal <$> natP

multitiveP :: ReadP Expr
multitiveP  -- using applicative notation
    =   (ExprMul <$> decimalP <*> (char '*' *> multitiveP))
    <|> decimalP

additiveP :: ReadP Expr
additiveP   -- using do notation
    =   do
            e1 <- multitiveP
            _ <- char '+'
            e2 <- additiveP
            return $ ExprAdd e1 e2
    <|> multitiveP

