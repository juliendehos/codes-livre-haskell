module Mycalc.Parser.ReadpSpec (main, spec) where

import Test.Hspec
import Text.ParserCombinators.ReadP

import Mycalc.Parser.Readp
import Mycalc.Syntax

main :: IO ()
main = hspec spec

spec :: Spec
spec = do

    ---------------------------------------------------------------------------
    -- natP :: Parser Int
    ---------------------------------------------------------------------------

    describe "natP" $ do
        it "1" $ fst (last (readP_to_S natP "0")) `shouldBe` 0
        it "2" $ fst (last (readP_to_S natP "42")) `shouldBe` 42
        it "3" $ fst (last (readP_to_S natP "42foo")) `shouldBe` 42
        it "4" $ readP_to_S natP "foo42" `shouldBe` []

    ---------------------------------------------------------------------------
    -- decimalP :: Parser Expr
    ---------------------------------------------------------------------------

    describe "decimalP" $ do
        it "1" $ fst (last (readP_to_S decimalP "0")) `shouldBe` ExprVal 0
        it "2" $ fst (last (readP_to_S decimalP "42")) `shouldBe` ExprVal 42
        it "3" $ fst (last (readP_to_S decimalP "42foo")) `shouldBe` ExprVal 42
        it "4" $ readP_to_S decimalP "foo42" `shouldBe` []

    ---------------------------------------------------------------------------
    -- multitiveP :: Parser Expr
    ---------------------------------------------------------------------------

    describe "multitiveP" $ do
        it "1" $ fst (last (readP_to_S multitiveP "20*22")) `shouldBe`
                    ExprMul (ExprVal 20) (ExprVal 22)
        it "2" $ fst (last (readP_to_S multitiveP "20*22*2")) `shouldBe`
                    ExprMul (ExprVal 20) (ExprMul (ExprVal 22) (ExprVal 2))
        it "3" $ fst (last (readP_to_S multitiveP "20*foo")) `shouldBe` ExprVal 20
        it "4" $ readP_to_S multitiveP "foo*20" `shouldBe` []

    ---------------------------------------------------------------------------
    -- additiveP :: Parser Expr
    ---------------------------------------------------------------------------

    describe "additiveP" $ do
        it "1" $ fst (last (readP_to_S additiveP "20+22")) `shouldBe`
                    ExprAdd (ExprVal 20) (ExprVal 22)
        it "2" $ fst (last (readP_to_S additiveP "20*22")) `shouldBe`
                    ExprMul (ExprVal 20) (ExprVal 22)
        it "3" $ fst (last (readP_to_S additiveP "20+22+2")) `shouldBe`
                    ExprAdd (ExprVal 20) (ExprAdd (ExprVal 22) (ExprVal 2))
        it "2" $ fst (last (readP_to_S additiveP "20*10+12")) `shouldBe`
                    ExprAdd (ExprMul (ExprVal 20) (ExprVal 10)) (ExprVal 12)
        it "3" $ fst (last (readP_to_S additiveP "20+10*12")) `shouldBe`
                    ExprAdd (ExprVal 20) (ExprMul (ExprVal 10) (ExprVal 12))

    ---------------------------------------------------------------------------
    -- parseExpr :: String -> Either String Expr
    ---------------------------------------------------------------------------

    describe "ExprVal" $ do
        it "1" $ parseExpr "0" `shouldBe` Right (ExprVal 0)
        it "2" $ parseExpr "42" `shouldBe` Right (ExprVal 42)

    describe "ExprAdd" $ do
        it "1" $ parseExpr "20+22" `shouldBe`
                    Right (ExprAdd (ExprVal 20) (ExprVal 22))
        it "2" $ parseExpr "20+10+12" `shouldBe`
                    Right (ExprAdd (ExprVal 20) (ExprAdd (ExprVal 10) (ExprVal 12)))

    describe "ExprMul" $ do
        it "1" $ parseExpr "20*22" `shouldBe`
                    Right (ExprMul (ExprVal 20) (ExprVal 22))
        it "2" $ parseExpr "20*10+12" `shouldBe`
                    Right (ExprAdd (ExprMul (ExprVal 20) (ExprVal 10)) (ExprVal 12))
        it "3" $ parseExpr "20+10*12" `shouldBe`
                    Right (ExprAdd (ExprVal 20) (ExprMul (ExprVal 10) (ExprVal 12)))

