(* Définit une fonction qui calcule n!. *)
let rec factorielle = function
  | 0 -> 1
  | n -> n * factorielle (n - 1);;

(* Évalue la fonction sur le nombre 5. *)
print_endline (string_of_int (factorielle 5))
