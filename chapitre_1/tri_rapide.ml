(* Définit une fonction de tri. *)
let rec tri_rapide = function
   | [] -> []
   | pivot :: liste ->
       let liste_inf, liste_sup = List.partition (fun x -> x < pivot) liste in
         tri_rapide liste_inf @ [pivot] @ tri_rapide liste_sup;;

(* Appelle la fonction de tri sur une liste et affiche le résultat. *)
print_endline (String.concat " " 
                             (List.map string_of_int 
                                       (tri_rapide [3; 4; 1; 37; 42; 8; 13])))
