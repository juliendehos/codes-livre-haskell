-- Compteur initial (valeur non modifiable).
newCompteur = 0

-- Fonction qui retourne un nouveau compteur incrémenté.
incrementerCompteur compteur = compteur + 1

-- Utilisation : crée successivement un nouveau compteur à partir 
-- d'un ancien (pas de modification de variable).
main = putStrLn ("le compteur vaut " ++ show cpt)
  where cpt = incrementerCompteur (incrementerCompteur newCompteur)

