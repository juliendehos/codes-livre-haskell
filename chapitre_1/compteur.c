#include <stdio.h>

// Variable globale stockant la valeur du compteur.
int COMPTEUR = 0;

// Fonction qui modifie la valeur de la variable globale.
void incrementerCompteur() { COMPTEUR++; }

int main() {
  incrementerCompteur();  // Modifie la variable globale.
  incrementerCompteur();
  printf("le compteur vaut %d\n", COMPTEUR); // Lit sa valeur.
  return 0;
}

