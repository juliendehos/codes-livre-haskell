# Exemples du chapitre 1

## Exemples en C

```
gcc -o compteur compteur.c

./compteur 
le compteur vaut 2
```

## Exemples en Java

```
javac TestCompteur.java 

java TestCompteur
le compteur vaut 2
```


## Exemples en Haskell

```
runghc compteur.hs 
le compteur vaut 2
```


## Exemples en Lisp

```
sbcl --script factorielle.lisp 
120
```

## Exemples en Ocaml

```
ocaml factorielle.ml 
120
```

