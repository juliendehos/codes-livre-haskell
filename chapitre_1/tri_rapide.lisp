; Définit une fonction de tri.
(defun tri_rapide (liste &aux (pivot (car liste)) )
  (if (cdr liste)
      (nconc (tri_rapide (remove-if-not #'(lambda (x) (< x pivot)) liste))
             (remove-if-not #'(lambda (x) (= x pivot)) liste)
             (tri_rapide (remove-if-not #'(lambda (x) (> x pivot)) liste)))
      liste))

; Appelle la fonction de tri sur une liste et affiche le résultat.
(write (tri_rapide '(3 4 1 37 42 8 13)))
