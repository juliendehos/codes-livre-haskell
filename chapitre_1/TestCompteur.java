// Définit une classe implémentant le concept de compteur.
class Compteur {
  private int valeur;
  public Compteur() { valeur = 0; }
  public void incrementer() { valeur++; }
  public int getValeur() { return valeur; }
}

public class TestCompteur {
  public static void main(String[] args) {

    // Crée un objet cpt, de la classe Compteur.
    Compteur cpt = new Compteur();

    // Interagit avec l'objet cpt, grâce à ses fonctions.
    cpt.incrementer();
    cpt.incrementer();
    System.out.println("le compteur vaut " + cpt.getValeur());
  }
}

