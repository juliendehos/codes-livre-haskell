; Définit une fonction qui calcule n!.
(defun factorielle (n)
  (if (= n 0)
      1
      (* n (factorielle (- n 1)))))

; Évalue la fonction sur le nombre 5.
(write (factorielle 5))
