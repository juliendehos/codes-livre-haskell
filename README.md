# codes-livre-haskell

[Julien Dehos. La programmation fonctionnelle - Introduction et application en Haskell à l'usage de l'étudiant et du développeur, Ellipses, 2019.](https://www.editions-ellipses.fr/programmation-fonctionnelle-introduction-applications-haskell-lusage-ltudiant-dveloppeur-p-13083.html)

## Articles inspirés du livre

- [Apprendre comment implémenter une IA de jeu en Haskell, Developpez.com, 2019](https://juliendehos.developpez.com/tutoriels/haskell/implementer-ia-jeu-en-Haskell/)
- [Apprendre comment implémenter un serveur de blog avec le langage Haskell, Developpez.com, 2019](https://juliendehos.developpez.com/tutoriels/haskell/implementer-serveur-blog/)


## Références de l'avant-propos

- [Haskell Platform.](https://www.haskell.org/downloads)
- [Exercices de programmation fonctionnelle.](https://juliendehos.gitlab.io/posts/pf/index.html)
- [Exercices de prog. fonctionnelle pour le web.](https://juliendehos.gitlab.io/posts/pfw/index.html)


## Références du chapitre 1

- [John Hughes. Why Functional Programming Matters. Computer Journal, 32(2), 1989.](https://www.cs.kent.ac.uk/people/staff/dat/miranda/whyfp90.pdf)
- [Joe Armstrong. A History of Erlang. Conference on History of Programming Languages, HOPL III, 2007.](http://webcem01.cem.itesm.mx:8005/erlang/cd/downloads/hopl_erlang.pdf)
- [Yaron Minsky. OCaml for the masses. Communications of the ACM, September, 2011.](https://queue.acm.org/detail.cfm?id=2038036)
- [Bas van Dijk. Functional Programming at LumiGuide. Google TechTalk, ZuriHac 2016.](https://www.youtube.com/watch?v=IKznN_TYjZk)
- [Yoann Padioleau. Pfff : PHP Program analysis at Facebook. The OCaml Users and Developers Workshop, 2013.](https://ocaml.org/meetings/ocaml/2013/slides/padioleau.pdf)
- [Simon Marlow, Louis Brandy, Jonathan Coens, Jon Purdy. There is no fork : an abstraction for efficient, concurrent, and concise data access. International Conference on Functional programming, 49(9), 2014.](https://simonmar.github.io/bib/papers/haxl-icfp14.pdf)
- [Simon Marlow. Fighting Spam with Haskell. Imperial Programming Lectures, 2016.](http://multicore.doc.ic.ac.uk/iPr0gram/slides/2015-2016/Marlow-fighting-spam.pdf)


## Références du chapitre 2

- <https://www.haskell.org>
- `https://haskell-lang.org` : ce site a été [fermé](https://www.snoyman.com/blog/2019/02/shutting-down-haskell-lang)
- [Haskell Wikibook.](https://en.wikibooks.org/wiki/Haskell)
- [Learn You A Haskell for Great Good !](http://learnyouahaskell.com)
- [Real World Haskell.](http://book.realworldhaskell.org)
- [What I Wish I Knew When Learning Haskell.](http://dev.stephendiehl.com/hask)
- [Gabriel Gonzalez. State of the Haskell ecosystem.](https://github.com/Gabriel439/post-rfc/blob/master/sotu.md)
- [Hackage.](https://hackage.haskell.org)
- [Hoogle.](https://www.haskell.org/hoogle)


## Références du chapitre 3

- [Gabriel Gonzalez. State of the Haskell ecosystem.](https://github.com/Gabriel439/post-rfc/blob/master/sotu.md)
- [Graham Hutton, Erik Meijer. Monadic Parser Combinators. Rapport technique NOTTCS-TR-96-4, Université de Nottingham, 1996.](http://www.cs.nott.ac.uk/~pszgmh/monparsing.pdf)
- [Graham Hutton. Programming in Haskell. 2nd Edition, Cambridge University Press, 2016.](http://www.cs.nott.ac.uk/~pszgmh/pih.html)
- [Megaparsec: Monadic parser combinators.](https://hackage.haskell.org/package/megaparsec)
- [Hackage.](https://hackage.haskell.org/packages)
- [Glasgow Haskell Compiler.](https://github.com/ghc/ghc)
- [All About Monads.](https://wiki.haskell.org/All_About_Monads)
- [Brent Yorgey, Abstraction, intuition, and the "monad tutorial fallacy".](https://byorgey.wordpress.com/2009/01/12/abstraction-intuition-and-the-monad-tutorial-fallacy)
- [Typeclassopedia.](https://wiki.haskell.org/Typeclassopedia)
- [Bartosz Milewski, Category Theory for Programmers.](https://bartoszmilewski.com/2014/10/28/category-theory-for-programmers-the-preface/), disponible également en [PDF](https://github.com/hmemcpy/milewski-ctfp-pdf)
- [GHC Language Features.](https://downloads.haskell.org/~ghc/latest/docs/html/users_guide/glasgow_exts.html)
- [Haskell/Performance introduction.](https://en.wikibooks.org/wiki/Haskell/Performance_introduction)
- [Samuli Thomasson, Haskell High Performance Programming, Packt Publishing, 2016.](https://www.packtpub.com/application-development/haskell-high-performance-programming)


## Références du chapitre 4

- [The Haskell Cabal.](https://www.haskell.org/cabal)
- [The Haskell Tool Stack.](https://www.haskellstack.org)
- [Stackage.](https://www.stackage.org)
- [Nix. The Purely Functional Package Manager.](https://nixos.org/nix)
- [Haddock: A Haskell Documentation Tool.](https://www.haskell.org/haddock)
- [Hspec: A Testing Framework for Haskell.](https://hspec.github.io)
- [scotty: Haskell web framework inspired by Ruby's Sinatra, using WAI and Warp.](https://hackage.haskell.org/package/scotty)
- [Sinatra: a DSL for quickly creating web applications in Ruby with minimal effort.](http://sinatrarb.com)
- [SQLite: a self-contained, high-reliability, embedded, full-featured, public-domain, SQL database engine.](https://www.sqlite.org)
- [sqlite-simple: Mid-Level SQLite client library.](https://hackage.haskell.org/package/sqlite-simple)
- [lucid: Clear to write, read and edit DSL for HTML.](https://hackage.haskell.org/package/lucid)
- [Injection SQL.](https://fr.wikipedia.org/wiki/Injection_SQL)


## Références du chapitre 5

- [News, Status & Discussion about Standard C++.](https://isocpp.org)
- [Bjarne Stroustrup. The C++ Programming Language. Addison-Wesley, 1985.](http://www.stroustrup.com/1st.html)
- [Margaret A. Ellis, Bjarne Stroustrup. The Annotated C++ Reference Manual. Addison Wesley, 1990.](http://www.stroustrup.com/arm.html)
- [Alexander Stepanov, Meng Lee. The Standard Template Library. HP Laboratories Technical Report 95-11, 1995.](http://stepanovpapers.com/STL/DOC.PDF)
- [ISO/IEC 14882 : 2011. Information technology - Programming languages - C++.](https://www.iso.org/standard/50372.html)
- <https://github.com/juliendehos/runcpp>
- [Mozilla Developer Network - JavaScript.](https://developer.mozilla.org/fr/docs/Web/JavaScript)
- [ECMA-262 6th Edition, The ECMAScript 2015 Language Specification.](https://www.ecma-international.org/ecma-262/6.0/ECMA-262.pdf)
- [Node.js.](https://nodejs.org)
- [Python.](https://www.python.org)
- [Julia.](https://julialang.org)
- [Rust.](https://www.rust-lang.org)


## Références de la conclusion

- [Autoquizer](https://gitlab.com/juliendehos/autoquizer) : A simple web app for running quizzes + automatic scoring.
- [Haskeroku](https://gitlab.com/juliendehos/haskeroku) : A sample Haskell webapp deployed on Heroku.


