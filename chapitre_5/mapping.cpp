#include <algorithm>
#include <iostream>
#include <numeric>
#include <vector>
using namespace std;

void afficher(const vector<int> & v) {
  for_each(begin(v), end(v), [](int x){ cout << x << " "; });
  cout << endl;
}

int main() {
  vector<int> v {7, 42, 13, 37};

  // Mapping.
  vector<int> v_double;
  transform(begin(v), end(v), back_inserter(v_double), 
      [](int x){ return x*2; });
  afficher(v_double);

  // Filtrage.
  vector<int> v_impair;
  copy_if(begin(v), end(v), back_inserter(v_impair), 
      [](int x){ return x%2==1; });
  afficher(v_impair);

  // Réduction.
  int v_somme = accumulate(begin(v), end(v), 0, plus<int>());
  cout << v_somme << endl;
  return 0;
}

