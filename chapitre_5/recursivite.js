"use strict";

// Fonction récursive qui calcule Fibonacci.
function fibo(n, m1=1, m2=0) {
  return n == 0 ? m2 : fibo(n-1, m1+m2, m1);
}
for (let i=3; i<6; i++)
  console.log(`fibo de ${i} vaut ${fibo(i)}`);

// Fonction récursive qui affiche un tableau.
function afficher(d) {
  if (d.length != 0) {
    const [x, ...xs] = d;
    console.log(x);
    afficher(xs);
  }
}
afficher( [7, 42, 23] );

