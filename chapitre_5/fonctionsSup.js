"use strict";
// Prend une fonction en paramètre (formater).
function afficher1(n, formater) {
  console.log("(afficher1)", formater(n));
}
// Passe une fonction flêchée en paramètre.
afficher1(12, x => `x = ${x}`);
afficher1(12, x => `la valeur de x est : ${x}`);

// Prend deux fonctions en paramètres (formater et afficher).
function afficher2(afficher, n, formater) {
  afficher("(afficher2)", formater(n));
}
// Passe une fonction classique et une fonction flêchée.
afficher2(console.log, 12, x => `x: ${x}`);

