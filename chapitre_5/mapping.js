"use strict";

const v = [7, 42, 13, 37];

// Mapping.
const v_double = v.map(x => x*2);
console.log("map:", v_double);

// Filtrage.
const v_impair = v.filter(x => x%2 == 1);
console.log("filter:", v_impair);

// Filtrage avec une boucle for.
let v_pair = [];
for (let i=0; i<v.length; i++) {
  const vi = v[i];
  if (vi % 2 == 0) v_pair.push(vi);
}
console.log("filter_for:", v_pair);

// Réduction avec la valeur initiale par défaut.
const v_somme = v.reduce( (acc, x) => acc+x );
console.log("reduce:", v_somme);

// Succession de traitements.
const v_sup20_neg = v.filter(x => x>20)
                     .map(x => -x);
console.log("filter+map:", v_sup20_neg);

