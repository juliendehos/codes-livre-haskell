#include <iostream>
#include <tuple>

int main() {
  auto t = std::make_tuple<int, std::string>(42, "toto");
  int i;
  std::string s;
  std::tie(i, s) = t;  // Déconstruit t dans i et s.
  std::cout << i << " " << s << std::endl;
  return 0;
}

