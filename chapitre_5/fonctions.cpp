#include <functional>
#include <iostream>

int ajouter(int x, int y) { return x + y; }

int main() {

  // Fonction classique.
  std::function<int(int,int)> f1 = ajouter;
  std::cout << "f1(1, 2): " << f1(1, 2) << std::endl;

  // Fonction lambda.
  auto f2 = [](int x, int y) { return x + y; };
  std::cout << "f2(3, 4): " << f2(3, 4) << std::endl;

  // Objet-fonction de la STL.
  auto f3 = std::plus<int>();
  std::cout << "f3(5, 6): " << f3(5, 6) << std::endl;

  // Liaison de paramètres sur la fonction ajouter.
  auto f4 = std::bind(ajouter, std::placeholders::_1, 42);
  std::cout << "f4(1): " << f4(1) << std::endl;
  return 0;
}

