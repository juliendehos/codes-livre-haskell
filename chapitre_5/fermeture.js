"use strict";

// addN utilise une variable n extérieure à la fonction.
function addN(x) { return x + n; }

// makeAddN construit une fermeture qui inclut sa variable n.
function makeAddN(n) { return x => x+n; }

let n = 1;
const add1 = makeAddN(n);
console.log("addN(10) quand n=1:", addN(10));
console.log("add1(10) quand n=1:", add1(10));

n = 2;
console.log("addN(10) quand n=2:", addN(10));
console.log("add1(10) quand n=2:", add1(10));

