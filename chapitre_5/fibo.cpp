#include <iostream>

// Implémente Fibonacci selon la définition :
// F(n) = F(n-1) + F(n-2).
template<int N> struct fibonacci {
  static constexpr int v = fibonacci<N-1>::v + fibonacci<N-2>::v;
};

// Cas terminaux.
template<> struct fibonacci<0> { static constexpr int v = 0; };
template<> struct fibonacci<1> { static constexpr int v = 1; };

int main() {
  // Demande l'évaluation de fibonacci pour la valeur 5 puis 6.
  std::cout << fibonacci<5>::v << std::endl;
  std::cout << fibonacci<6>::v << std::endl;
  return 0;
}

