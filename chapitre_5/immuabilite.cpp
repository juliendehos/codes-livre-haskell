#include <iostream>
#include <vector>

void afficher(const std::vector<int> & v) {
  // Le paramètre v est une référence non modifiable.
  for (int x : v) std::cout << x << " ";
  std::cout << std::endl;
}

void afficher_modif(std::vector<int> v) {
  // Le paramètre v est une copie locale, modifiable.
  for (int & x : v) x++;  // Modifie les valeurs de v.
  afficher(v);
}  // La copie locale v est supprimée.

int main() {
  const std::vector<int> t1 {1, 2, 3};
  afficher_modif(t1);  // Ok car le paramètre est copié.
  afficher(t1);        // Ok car référence constante.
  return 0;
}

