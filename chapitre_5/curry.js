"use strict";

// Fonction non curryfiée.
const add = function(x, y) { return x+y; }
console.log("add:", add(1, 2));

// Fonction curryfiée.
const add_curry = function(x) {
  return function(y) { return x+y; }
}
console.log("add_curry:", add_curry(3)(4));

// Évaluation partielle d'une fonction curryfiée.
const add1 = add_curry(1);
console.log("add1:", add1(5));

// Idem mais avec des fonctions flêchées...
const add_f = (x, y) => x+y;
console.log("add_f:", add_f(1, 2));

const add_curry_f = x => y => x+y;
console.log("add_curry_f:", add_curry_f(3)(4));

const add1_f = add_curry_f(1);
console.log("add1_f:", add1_f(5));

