package main

import "fmt"
import "io/ioutil"
import "math"
import "os"
import "strconv"
import "strings"

type Stats struct {
  Avg float64
  Var float64
  Algo string
  Params []float64
}

// Calcule des stats sur un ensemble de données.
func computeStats(data []float64, algo string,
                  params []float64) Stats {
  var n float64 = float64(len(data))
  var myAvg, myVar float64 = 0, 0
  // Calcule la moyenne.
  for _, v := range data { myAvg += v }
  myAvg /= n
  // Calcule la variance.
  for _, v := range data { myVar += math.Pow(myAvg - v, 2) }
  myVar /= n
  // Retourne les stats.
  return Stats{myAvg, myVar, algo, params}
}

// Affiche des stats.
func printStats(s Stats) {
  // Formate les paramètres (intercale un "|").
  ps := make([]string, len(s.Params))
  for i, x := range s.Params { ps[i] = fmt.Sprintf("%f", x) }
  params := strings.Join(ps, "|")
  // Affichage.
  fmt.Printf("%f; %f; %s; %s\n", s.Avg, s.Var, s.Algo, params)
}

// Lit les réels d'un fichier.
func readParseFile(filename string) []float64 {
  data0, _ := ioutil.ReadFile(filename)
  data1 := make([]float64, 0)
  for _, line := range strings.Split(string(data0), "\n") {
    x, e := strconv.ParseFloat(line, 64)
    if e == nil { data1 = append(data1, x) }
  }
  return data1
}

// Construit et retourne une fonction, ou un message d'erreur.
func createFunc(algo string, ps []float64) func(float64) float64 {
  n := len(ps)
  switch algo {
    case "mul2" :
      if n != 0 { panic("args for x*2: mul2") }
      return func(x float64) float64 { return x * 2 }
    case "mul" :
      if n != 1 { panic("args for x*k: mul <k>") }
      k := ps[0]
      return func(x float64) float64 { return x * k }
    case "sin" :
      if n != 2 { panic("args for sin(a*x+b): sin <a> <b>") }
      a, b := ps[0], ps[1]
      return func(x float64) float64 { return math.Sin(x*a + b) }
  }
  panic("unknown algo")
}

// Convertit un tableau de string en tableau de float64.
func parseParams(xs []string) []float64 {
  ys := make([]float64, len(xs))
  for i, x := range xs {
    y, _ := strconv.ParseFloat(x, 64)
    ys[i] = y
  }
  return ys
}

// Applique une fonction sur un tableau (en float64).
func mapFunc(xs []float64, f func(float64) float64) []float64 {
  ys := make([]float64, len(xs))
  for i, x := range xs { ys[i] = f(x) }
  return ys
}

func main() {
  if len(os.Args) < 3 {
    fmt.Println("usage:", os.Args[0], "<input> <algo> <params>")
    os.Exit(-1)
  }
  filename := os.Args[1]
  algo := os.Args[2]
  params := parseParams(os.Args[3:])
  f := createFunc(algo, params)
  data1 := readParseFile(filename)
  data2 := mapFunc(data1, f)
  stats := computeStats(data2, algo, params)
  println("average; variance; algo; params")
  printStats(stats)
}

