import Data.List (intercalate)
import System.Environment (getArgs, getProgName)
import System.Exit (exitSuccess, exitFailure)
import Text.Printf (printf)

data Stats = Stats
  { statsAvg    :: Double
  , statsVar    :: Double
  , statsAlgo   :: String
  , statsParams :: [Double]
  }

-- Calcule des stats sur un ensemble de données.
computeStats :: [Double] -> String -> [Double] -> Stats
computeStats xs algo params = Stats avg var algo params
  where n = fromIntegral $ length xs
        avg = (sum xs) / n
        facc acc x = acc + (x - avg)**2
        var = (foldl facc 0 xs) / n

-- Affiche des stats.
instance Show Stats where
  show (Stats avg var algo params) = 
    printf "%.4f; %.4f; %s; %s" avg var algo (format params)
    where format = intercalate "|" . map show

-- Lit les réels d'un fichier.
readAndParse :: String -> IO [Double]
readAndParse fname = readFile fname >>= return . map read . words 

-- Construit et retourne une fonction, ou un message d'erreur.
createFunc :: String -> [Double] -> Either String (Double->Double)
createFunc "mul2" xs
  | length xs /= 0 = Left "args for x*2: mul2"
  | otherwise = Right (*2)
createFunc "mul" xs
  | length xs /= 1 = Left "args for x*k: mul <k>"
  | otherwise = Right $ (*) (head xs)
createFunc "sin" xs
  | length xs /= 2 = Left "args for sin(a*x+b): sin <a> <b>"
  | otherwise = let [a,b] = xs in Right $ \x -> sin(x*a + b)
createFunc _ _ = Left "unknown algo"

-- Programme principal.
main :: IO ()
main = do
  args <- getArgs
  if length args < 2
  then do
    progName <- getProgName
    putStrLn $ "usage: " ++ progName ++ " <input> <algo> <params>"
    exitFailure
  else do
    let (filename:algo:paramsStr) = args
        params = map read paramsStr :: [Double]
        funcE = createFunc algo params
    case funcE of
      Left err -> putStrLn err >> exitFailure
      Right func -> do
        data1 <- readAndParse filename
        let data2 = map func data1
        putStrLn "average; variance; algo; params"
        print $ computeStats data2 algo params
        exitSuccess

