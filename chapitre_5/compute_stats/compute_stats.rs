use std::env;
use std::fmt;
use std::fs::File;
use std::io;
use std::io::prelude::*;

struct Stats {
  avg: f64,
  var: f64,
  algo: String,
  params: Vec<f64>
}

// Calcule des stats sur un ensemble de données.
fn compute_stats(data: &[f64], algo: &str, 
                 params: Vec<f64>) -> Stats {
  let n = data.len() as f64;
  let avg = data.iter().sum::<f64>() / n;
  let var = 
    data.iter().fold(0.0, |acc, x| acc+(x-avg).powf(2.0)) / n;
  Stats {avg, var, algo: algo.into(), params}
}

// Affiche des stats.
impl fmt::Display for Stats {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
    let ps = self.params
      .iter()
      .map(ToString::to_string)
      .collect::<Vec<_>>()
      .join("|");
    writeln!(f, "{};{};{};{}", self.avg, self.var, self.algo, ps)
  }
}

// Lit les réels d'un fichier.
fn read_parse_file(filename: &str) -> Vec<f64> {
  let contents = &mut String::new();
  let mut file = File::open(filename).unwrap();
  file.read_to_string(contents).unwrap();
  contents.split_whitespace()
    .map(|x| x.parse::<f64>().unwrap())
    .collect()
}

// Lit les réels d'un fichier (avec gestion d'erreur).
fn read_parse_file2(filename: &str) -> io::Result<Vec<f64>> {
  let contents = &mut String::new();
  let mut file = File::open(filename)?;
  file.read_to_string(contents)?;
  let err_f = |e| io::Error::new(io::ErrorKind::InvalidData, e);
  contents.split_whitespace()
    .map(|x| x.parse::<f64>().map_err(err_f))
    .collect()
}

// Construit et retourne une fonction, ou un message d'erreur.
fn create_func(algo: &str, params: &[f64]) -> 
Result<Box<Fn(&f64) -> f64>, String> {
  let n = params.len();
  Ok(match algo {
    "mul2" => 
      if n!=0 { Err("args for x*2: mul2")? }
      else { Box::new(|x| x * 2.0) }
    "mul" => 
      if n!=1 { Err("args for x*k: mul <k>")? }
      else { 
        let k = params[0];
        Box::new(move |x| x * k) 
      }
    "sin" => 
      if n!=2 { Err("args for sin(a*x+b): sin <a> <b>")? }
      else { 
        let a = params[0];
        let b = params[1];
        Box::new(move |x| (x * a + b).sin())
      }
    _ => Err("unknown algo")?
  })
}

fn main() -> Result<(), Box<std::error::Error>> {
  let args: Vec<String> = env::args().collect();
  if args.len() < 3 {
    return Err(format!("usage: {} <input> <algo> <params>", 
                       args[0]).into());
  }

  let filename = &args[1];
  let algo = &args[2];

  let params = (&args[3..])
    .iter()
    .map(|x| x.parse())
    .collect::<Result<Vec<f64>, _>>()?;

  let func = create_func(algo, &params)?;
  let data = read_parse_file(filename);
  let data = data.iter().map(|x| func(x)).collect::<Vec<_>>();
  let stats = compute_stats(&data, algo, params);

  println!("average; variance; algo; params");
  println!("{}", stats);
  Ok(())
}

