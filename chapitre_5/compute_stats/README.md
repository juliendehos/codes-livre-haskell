# compute_stats

Exemple d'application illustrant le style fonctionnel. Lit des nombres dans un
fichier, leur applique une fonction (donnée en paramètres de la ligne de
commande) et calcule quelques stats sur les résultats (moyenne, variance).

## implémentation Haskell

```
$ runghc compute_stats.hs input.txt mul2
average; variance; algo; params
3.7500; 2.4375; mul2; 

$ runghc compute_stats.hs input.txt mul 42
average; variance; algo; params
78.7500; 1074.9375; mul; 42.0

$ runghc compute_stats.hs input.txt sin 0.5 2
average; variance; algo; params
0.1897; 0.1366; sin; 0.5|2.0
```

## implémentation C++

```
$ g++ -std=c++17 -o compute_stats compute_stats.cpp 

$ ./compute_stats input.txt mul2
average; variance; algo; params
3.75; 2.4375; mul2; 

...
```


## implémentation JavaScript (Node.js)

```
$ node compute_stats.js input.txt mul2
average; variance; algo; params
3.75; 2.4375; mul2; 

...
```


## implémentation Python

```
$ ./compute_stats.py input.txt mul2
average; variance; algo; params
3.75; 2.4375; mul2; 

...
```


## implémentation Julia

```
$ ./compute_stats.jl input.txt mul2
average; variance; algo; params
3.750000; 2.437500; mul2; 

...
```


## implémentation Rust

```
$ cargo run input.txt mul2
average; variance; algo; params
3.75; 2.4375; mul2; 

...
```

