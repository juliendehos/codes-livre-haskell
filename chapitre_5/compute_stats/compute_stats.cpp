#include <algorithm>
#include <cmath>
#include <fstream>
#include <functional>
#include <iostream>
#include <numeric>
#include <sstream>
#include <variant>
#include <vector>
using namespace std;

struct Stats {
  double avg;
  double var;
  string algo;
  vector<double> params;
};

// Calcule des stats sur un ensemble de données.
Stats computeStats(const vector<double> & data, const string &algo, 
                   const vector<double> & params) {
  const double n = double(data.size());
  const double avg = accumulate(begin(data), end(data), 0.0) / n;
  auto fAcc = 
    [avg](double acc, double x){ return acc + pow(x - avg, 2); };
  const double var = 
    accumulate(begin(data), end(data), 0.0, fAcc) / n;
  return {avg, var, algo, params};
}

// Formate les paramètres de l'algo (intercale un '|').
string formatParams(const vector<double> & params) {
  auto str = accumulate(begin(params), end(params), string(),
      [](auto str, auto x) { return str + to_string(x) + "|"; });
  if (not str.empty()) str.pop_back();
  return str;
}

// Affiche des stats.
ostream & operator<<(ostream & os, const Stats & stats) {
  os << stats.avg << "; " << stats.var << "; "
    << stats.algo << "; " << formatParams(stats.params);
  return os;
}

// Lit les réels d'un fichier.
vector<double> readAndParseFile(const string & filename) {
  vector<double> data;
  ifstream ifs(filename);
  double x;
  while (ifs >> x) data.push_back(x);
  return data;
}

// Définit un alias de nom.
using func_t = function<double(double)>;

// Construit et retourne une fonction, ou un message d'erreur.
variant<string,func_t> createFunc(const string & algo, 
                                  const vector<double> & params) {
  const unsigned nParams = params.size();
  if (algo == "mul2") {
    if (nParams != 0) return "args for x*2: mul2";
    return [](double x) { return x*2; };
  } 
  else if (algo == "mul"){
    if (nParams != 1) return "args for x*k: mul <k>";
    const double k = params[0];
    return [k](double x) { return x*k; };
  }
  else if (algo == "sin") {
    if (nParams != 2) return "args for sin(a*x+b): sin <a> <b>";
    const double a = params[0];
    const double b = params[1];
    return [a,b](double x) { return sin(x*a + b); };
  }
  return "unknown algo";
}

// Programme principal.
int main(int argc, char ** argv) {

  if (argc < 2) {
    cout << "usage: " << argv[0] << " <input> <algo> <params>\n";
    exit(-1);
  }

  const string filename = argv[1];
  const string algo = argv[2];

  vector<double> params;
  for (int i=3; i<argc; i++) params.push_back(atof(argv[i]));

  auto funcV = createFunc(algo, params);
  if (holds_alternative<string>(funcV)) {
    cout << get<string>(funcV) << endl;
    exit(-1);
  }

  auto data1 = readAndParseFile(filename);
  vector<double> data2(data1.size());
  auto func = get<func_t>(funcV);
  transform(begin(data1), end(data1), begin(data2), func);

  auto stats = computeStats(data2, algo, params);
  cout << "average; variance; algo; params" << endl;
  cout << stats << endl;
  return 0;
}

