#!/usr/bin/env python3
import sys
from functools import reduce
from math import sin

def compute_stats(data, algo, params):
    ''' Calcule des stats sur un ensemble de données. '''
    n_params = float(len(data))
    avg = sum(data) / n_params
    f_acc = lambda acc, x: acc + (x - avg)**2
    var = reduce(f_acc, data, 0) / n_params
    return {'avg': avg, 'var': var, 'algo': algo, 'params': params}

def print_stats(stats):
    ''' Affiche les stats. '''
    params_str = '|'.join(map(str, stats['params']))
    avg, var, algo, _ = stats.values()
    print("{}; {}; {}; {}".format(avg, var, algo, params_str))

def read_parse_file(filename):
    ''' Lit les réels d'un fichier. '''
    with open(filename) as file:
        data_str = file.read().split()
    return map(float, data_str)

def create_func(algo, params):
    ''' Construit et retourne une fonction, ou une erreur. '''
    n_params = len(params)
    func = lambda x: x
    if algo == "mul2":
        if n_params != 0:
            return "args for x*2: mul2"
        func = lambda x: x*2
    if algo == "mul":
        if n_params != 1:
            return "args for x*k: mul <k>"
        k = params[0]
        func = lambda x: x*k
    if algo == "sin":
        if n_params != 2:
            return "args for sin(a*x+b): sin <a> <b>"
        k, j = params
        func = lambda x: sin(x*k+j)
    return func

if __name__ == "__main__":
    if len(sys.argv) < 3:
        print("usage:", sys.argv[0], "<input> <algo> <params>")
        sys.exit(-1)
    FILENAME = sys.argv[1]
    ALGO = sys.argv[2]

    PARAMS = list(map(float, sys.argv[3:]))
    STRORFUNC = create_func(ALGO, PARAMS)
    if isinstance(STRORFUNC, str):
        print(STRORFUNC)
        sys.exit(-1)

    DATA1 = read_parse_file(FILENAME)
    DATA2 = list(map(STRORFUNC, DATA1))
    STATS = compute_stats(DATA2, ALGO, PARAMS)
    print("average; variance; algo; params")
    print_stats(STATS)
