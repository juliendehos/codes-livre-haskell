"use strict";

// Calcule des stats sur un ensemble de données.
function computeStats(data, algo, params) {
  const n = data.length;
  const average = data.reduce((acc, x) => acc+x, 0) / n;
  const fAcc = (acc, x) => acc + Math.pow(x-average, 2);
  const variance = data.reduce(fAcc, 0) / n;
  return {average, variance, algo, params};
}

// Affiche des stats.
function formatStats(stats) {
  const {average, variance, algo, params} = stats;
  const paramsStr = params.join("|");
  return `${average}; ${variance}; ${algo}; ${paramsStr}`;
}

// Lit les réels d'un fichier.
function readAndParseFile(filename) {
  const fs = require("fs");
  const dataStr = fs.readFileSync(filename, "utf8");
  return dataStr.split("\n").filter(Number).map(parseFloat);
}

// Construit et retourne une fonction, ou un message d'erreur.
function createFunc(algo, params) {
  const n = params.length;
  switch (algo) {
    case "mul2":
      if (n != 0) return "args for x*2: mul2";
      return x => x*2;
    case "mul":
      if (n != 1) return "args for x*k: mul <k>";
      const [k] = params;
      return x => x*k;
    case "sin":
      if (n != 2) return "args for sin(a*x+b): sin <a> <b>";
      const [a,b] = params;
      return x => Math.sin(x*a + b);
    default:
      return "unknown algo";
  }
}

// Programme principal.
const args = process.argv.slice(2);
if (args.length < 2) {
  const path = require("path");
  const progName = path.basename(process.argv[1]);
  console.log(`usage: node ${progName} <input> <algo> <params>`);
  process.exit(-1);
}
const [filename, algo, ...paramsStr] = args;

const params = paramsStr.map(parseFloat);
const func = createFunc(algo, params);
if (typeof(func) === "string") {
  console.log(func);
  process.exit(-1);
}

const data1 = readAndParseFile(args[0]).map(func);
const stats = computeStats(data1, algo, params);
console.log("average; variance; algo; params");
console.log(formatStats(stats));

