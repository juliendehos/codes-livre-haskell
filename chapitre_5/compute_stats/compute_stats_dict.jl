#!/usr/bin/env julia

# Calcule des stats sur un ensemble de données.
function computeStats(data::Vector{Float64}, algo, params)
  const myAvg = mean(data)
  const myVar = var(data, corrected=false)
  Dict("avg"=>myAvg, "var"=>myVar, "algo"=>algo, "params"=>params)
end

#--------------------------------------------------------------------

# Affiche des stats.
function printStats(stats)
  const pStr = join(map(string, stats["params"]), "|")
  @printf("%f; %f; %s; %s\n", stats["avg"], stats["var"], stats["algo"], pStr)
end

# Lit les réels d'un fichier.
readParseFile = vec ∘ readdlm    # Composition (digraphe Ob).

#--------------------------------------------------------------------

# Construit et retourne une fonction, ou un message d'erreur.
function createFunc(algo::String, prms::Vector{Float64})
  if (algo == "mul2")
    length(prms) != 0 ? "args for x*2: mul2" : x -> x*2
  elseif (algo == "mul")
    length(prms) != 1 ? "args for x*k: mul <k>" : x -> x*prms[1]
  elseif (algo == "sin")
    length(prms) != 2 ?
    "args for sin(a*x+b): sin <a> <b>" : x -> sin(x*prms[1] + prms[2])
  else
    "unknown algo"
  end
end

#--------------------------------------------------------------------

# Programme principal.
if (!isinteractive())
  if (length(ARGS) < 2)
    const prog = basename(Base.source_path())
    println("usage: ./", prog, " <input> <algo> <params>")
    exit(-1)
  end
  const filename, algo = ARGS
  const params = map(x -> parse(Float64, x), ARGS[3:end])

  strorfunc = createFunc(algo, params)
  if (typeof(strorfunc) == String)
    println(strorfunc)
    exit(-1)
  else
    const data1 = readParseFile(filename)
    const data2 = map(strorfunc, data1)
    const stats = computeStats(data2, algo, params)
    println("average; variance; algo; params")
    printStats(stats)
  end
end

