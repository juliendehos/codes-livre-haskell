"use strict";

function ajouter_impur(data, n) {
  // Push modifie le paramètre data (impur).
  data.push(n);
  return data;
}
const impur1 = [7, 42];
const impur2 = ajouter_impur(impur1, 23);
console.log("impur1:", impur1);
console.log("impur2:", impur2);

function ajouter_pur(data, n) {
  // La déconstruction copie le paramètre data (pur).
  return [...data, n];
}
const pur1 = [7, 42];
const pur2 = ajouter_pur(pur1, 23);
console.log("pur1:", pur1);
console.log("pur2:", pur2);

