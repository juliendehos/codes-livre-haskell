#include <algorithm>
#include <iostream>
#include <vector>

// Fonction récursive qui calcule un élément de Fibonacci.
int fibo(int n, int m1=1, int m2=0) {
  return n == 0 ? m2 : fibo(n-1, m1+m2, m1);
}

// Fonction récursive qui affiche un tableau.
void afficher(std::vector<int>::const_iterator it,
              std::vector<int>::const_iterator itEnd) {
  if (it != itEnd) {
    std::cout << *it << " ";
    afficher(++it, itEnd);
  }
}

int main() {

  // Teste fibo.
  for (int i=3; i<6; i++)
    std::cout << "fibo(" << i << ") = " << fibo(i) << std::endl;

  // Teste afficher.
  std::vector<int> v {7, 42, 13};
  afficher(begin(v), end(v));

  return 0;
}

