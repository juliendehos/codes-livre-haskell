#include <algorithm>
#include <iostream>

int main() {

  // Fonction non curryfiée.
  auto add = [](int x, int y){ return x+y; };
  std::cout << "add: " << add(1, 2) << std::endl;

  // Fonction curryfiée.
  auto add_curry = [](int x){ return [x](int y){ return x+y; }; };
  std::cout << "add_curry: " << add_curry(3)(4) << std::endl;

  // Évaluation partielle d'une fonction curryfiée.
  auto add1 = add_curry(1);
  std::cout << "add1: " << add1(5) << std::endl;
  return 0;
}

