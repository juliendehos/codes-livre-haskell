#include <iostream>

int main() {

  int n = 1;

  // Capture n par référence (fonction impure).
  auto addN_ref = [&n](int x) { return x+n; }; 

  // Capture n par valeur (fonction pure).
  auto addN_val = [n](int x) { return x+n; }; 

  n = 2;
  std::cout << "addN_ref: " << addN_ref(10) << std::endl;
  std::cout << "addN_val: " << addN_val(10) << std::endl;
  return 0;
}

