#include <algorithm>
#include <functional>
#include <iostream>
#include <vector>

// Prend une fonction en paramètre (formater).
void afficher(int n, std::function<std::string(int)> formater) {
  std::cout << "(afficher) " << formater(n) << std::endl;
}

int main() {
  // Construit une fonction et la passe à afficher.
  auto fFormater = [](int n){ return "-> " + std::to_string(n); };
  afficher(42, fFormater);

  // Construit une fonction (fCmp) et la passe à sort.
  std::vector<int> v {7, 42, 13, 37};
  auto fCmp = [](int x, int y) { return x>y; };
  std::sort(begin(v), end(v), fCmp);
  for (int x : v) std::cout << x << " ";
  return 0;
}

