"use strict";

// Définition de fonction classique.
function ajouter1(x, y) {
  return x + y;
}
console.log("ajouter1:", ajouter1(1, 2));

// Définition avec affectation.
const ajouter2 = function(x, y) {
  return x + y;
}
console.log("ajouter2:", ajouter2(3, 4));

// Fonction flêchée.
const ajouter3 = (x, y) => x + y;
console.log("ajouter3:", ajouter3(5, 6));

// Fonction flêchée à un paramètre.
const doubler = x => x * 2;
console.log("doubler:", doubler(3));

