"use strict";

// Construit un tableau puis le déconstruit explicitement.
const data1 = [ "hello", "world" ];
const [ str1, str2 ] = data1;
console.log("str1 str2:", str1, str2);

// Déconstruction tête + queue.
const data2 = [ 7, 42, 13, 37 ];
const [ x, ...xs ] = data2;
console.log("x xs:", x, xs);

// Déconstruit un objet partiellement.
const data3 = { key: 42, value: "toto", other: {} };
const { value: v, key } = data3;
console.log("key v:", key, v);

