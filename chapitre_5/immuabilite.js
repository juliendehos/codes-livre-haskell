"use strict";

// Une variable const est modifiable.
const data1 = [7];
data1.push(42);
console.log("data1:", data1);

// Seule la réaffectation est interdite
// (data1 = ...).

// L'affectation ne fait pas de copie, mais la déconstruction si.
const data2 = data1;
const data3 = [...data1];
data2[0] = 37;
console.log("data1:", data1);
console.log("data2:", data2);
console.log("data3:", data3);

