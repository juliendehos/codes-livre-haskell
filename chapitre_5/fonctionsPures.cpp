#include <algorithm>
#include <iostream>
#include <vector>

void ajouter_impur(std::vector<int> & v, int n) {
  // Modifie le paramètre.
  v.push_back(n);
}

std::vector<int> ajouter_pur(const std::vector<int> & v, int n) {
  // Modifie une copie du paramètre.
  std::vector<int> v2 = v;
  v2.push_back(n);
  return v2;
}

void afficher(const std::vector<int> & v) {
  // Ne modifie pas le paramètre mais réalise des sorties.
  for (int x : v) std::cout << x << " ";
  std::cout << std::endl;
}

int main() {
  std::vector<int> impur1 {7, 42};
  ajouter_impur(impur1, 23);
  afficher(impur1);

  std::vector<int> pur1 {7, 42};
  std::vector<int> pur2 = ajouter_pur(pur1, 23);
  afficher(pur1);
  afficher(pur2);
  return 0;
}

