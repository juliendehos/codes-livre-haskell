## avec Stack

```
stack setup
stack build
```
 puis

```
stack exec parsing1
```

ou

```
$ stack --nix exec ghci
GHCi, version 8.2.2: http://www.haskell.org/ghc/  :? for help

Prelude> :load parsing1
[1 of 1] Compiling Main             ( parsing1.hs, interpreted )
Ok, one module loaded.

*Main> parseTest parser5 "(test)"
"test"

...
```


## avec Nix

```
nix-shell --run "cabal run"
```

ou

```
$ nix-shell --run ghci
GHCi, version 8.2.2: http://www.haskell.org/ghc/  :? for help

Prelude> :load parsing1
[1 of 1] Compiling Main             ( parsing1.hs, interpreted )
Ok, one module loaded.

*Main> parseTest parser5 "(test)"
"test"

...
```

