import Data.Void
import Text.Megaparsec
import Text.Megaparsec.Char

parser0 :: Parsec Void String Char
parser0 = char '('

parser1 :: Parsec Void String Char
parser1 = notChar '('

parser2 :: Parsec Void String Char
parser2 = noneOf "([{<"

parser3 :: Parsec Void String String
parser3 = string "test" <* newline

parser4 :: Parsec Void String String
parser4 = between (string "test") newline (some printChar)

parser5 :: Parsec Void String Char
parser5 = char '{' <|> char '('

parser6 :: Parsec Void String String
parser6 = try (string "bon" <* newline) <|> string "bonjour"


{-

*Main> parseTest parser0 "(test)"
'('
*Main> parseTest parser0 "test"
1:1:
unexpected 't'
expecting '('

*Main> parseTest parser1 "test"
't'
*Main> parseTest parser1 "(test)"
1:1:
unexpected '('

*Main> parseTest parser2 "test"
't'
*Main> parseTest parser2 "(test)"
1:1:
unexpected '('

*Main> parseTest parser3 "test\n"
"test"
*Main> parseTest parser3 "test2\n"
1:5:
unexpected '2'
expecting newline


*Main> parseTest parser4 "test: hi world\n"
": hi world"
*Main> parseTest parser4 "test\n"
1:5:
unexpected newline
expecting printable character

*Main> parseTest parser5 "{("
'{'
*Main> parseTest parser5 "({"
'('
*Main> parseTest parser5 "test"
1:1:
unexpected 't'
expecting '(' or '{'


*Main> parseTest parser6 "bon\n"
"bon"
*Main> parseTest parser6 "bonjour"
"bonjour"

-}

