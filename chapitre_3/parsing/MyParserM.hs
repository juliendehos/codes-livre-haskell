module MyParserM where

data Parser a = Parser (String -> [(a, String)])

parse :: Parser a -> String -> [(a, String)]
parse (Parser p) input = p input

itemP :: Parser Char 
itemP = Parser (\inp -> case inp of
                          [] -> []
                          (x:xs) -> [(x, xs)])

charP :: Char -> Parser Char 
charP c = Parser (\inp -> case inp of
                          [] -> []
                          (x:xs) -> if x==c then [(x, xs)] else [])

instance Functor Parser where
  -- fmap :: (a -> b) -> Parser a -> Parser b
  fmap f p = Parser (\inp -> case parse p inp of
                               [] -> []
                               [(v, out)] -> [(f v, out)])

instance Applicative Parser where
  -- pure :: a -> Parser a
  pure v = Parser (\inp -> [(v,inp)])

  -- <*> :: Parser (a -> b) -> Parser a -> Parser b
  pg <*> px = Parser (\inp -> case parse pg inp of
                                [] -> []
                                [(g,out)] -> parse (fmap g px) out)

instance Monad Parser where
  -- (>>=) :: Parser a -> (a -> Parser b) -> Parser b
  p >>= f = Parser (\inp -> case parse p inp of
                              [] -> []
                              [(v,out)] -> parse (f v) out)

parenthesesP :: Parser Char
parenthesesP = do 
  charP '('  -- Reconnait un caractère '(',
  y <- itemP -- puis un caractère quelconque (et l'extrait dans y),
  charP ')'  -- puis un caractère ')',
  return y   -- puis retourne le caractère contenu dans y.

parenthesesP' :: Parser Char
parenthesesP' = charP '(' *> itemP <* charP ')'

data TwoChars = TwoChars Char Char deriving Show

twoCharsP :: Parser TwoChars
twoCharsP = TwoChars <$> parenthesesP <*> parenthesesP

main = do
  print $ parse itemP "toto"
  print $ parse (charP 't') "toto"
  print $ parse parenthesesP "(a)"
  print $ parse parenthesesP' "(a)"
  print $ parse twoCharsP "(a)(b)"

