import Data.Void
import Text.Megaparsec
import Text.Megaparsec.Char

parser0 :: Parsec Void String Char
parser0 = char '('

-- Style monadique.
parser1m :: Parsec Void String Char
parser1m = do
  char '('
  char ')'

-- Style applicatif.
parser1a :: Parsec Void String Char
parser1a = char '(' *> char ')'

parser2m :: Parsec Void String Char
parser2m = do
  char '('
  letterChar
  char ')'

parser2a :: Parsec Void String Char
parser2a = char '(' *> letterChar *> char ')'

parser3m :: Parsec Void String Char
parser3m = do
  char '('
  c <- letterChar
  char ')'
  return c

parser3a :: Parsec Void String Char
parser3a = char '(' *> letterChar <* char ')'

parser4m :: Parsec Void String String
parser4m = do
  char '('
  t <- many letterChar
  char ')'
  return t

parser4a :: Parsec Void String String
parser4a = char '(' *> many letterChar <* char ')'

parser5 :: Parsec Void String String
parser5 = between (char '(') (char ')') (many letterChar)


{-

*Main> parseTest parser0 "(test)"
'('
*Main> parseTest parser0 "test"
1:1:
unexpected 't'
expecting '('

*Main> parseTest parser1m "()"
')'
*Main> parseTest parser1a "()"
')'

*Main> parseTest parser2m "(a)"
')'
*Main> parseTest parser2a "(a)"
')'

*Main> parseTest parser3m "(a)"
'a'
*Main> parseTest parser3a "(a)"
'a'

*Main> parseTest parser4m "(test)"
"test"
*Main> parseTest parser4a "(test)"
"test"

*Main> parseTest parser5 "(test)"
"test"

-}

main :: IO ()
main = parseTest parser5 "(test)"

