module MyParser where

type Parser a = String -> [(a, String)]

letterP :: Parser Char 
letterP = \input -> case input of
  []     -> []
  (x:xs) -> if 'a'<=x && x<='z' then [(x, xs)] else []

letterP' :: Parser Char 
letterP' [] = []
letterP' (x:xs) = if 'a'<=x && x<='z' then [(x, xs)] else []

charP :: Char -> Parser Char 
charP _ [] = []
charP c (x:xs) = if x==c then [(c, xs)] else []

plusP :: Parser a -> Parser a -> Parser a
plusP p0 p1 input = p0 input ++ p1 input

seqP :: Parser a -> Parser a -> Parser [a]
seqP p0 p1 i0 = [([x0, x1], i2) | (x0, i1) <- p0 i0, 
                                  (x1, i2) <- p1 i1] 

main :: IO ()
main = do
  putStrLn "letterP"
  print $ letterP "hello"
  print $ letterP "42"

  putStrLn "charP"
  print $ (charP 'h') "hello"
  print $ (charP '4') "hello"

  putStrLn "plusP"
  print $ (letterP `plusP` charP '4') "hello"
  print $ (letterP `plusP` charP '4') "42"
  print $ (letterP `plusP` charP 'h') "hello"

  putStrLn "seqP"
  print $ (letterP `seqP` charP '4') "hello"
  print $ (letterP `seqP` charP 'e') "hello"

