pythagore :: Int -> [(Int,Int,Int)]
pythagore n = do
  a <- [1..n]
  b <- [a..n]
  c <- [b..n]
  if a^2+b^2 == c^2 then [(a,b,c)] else []

main = print $ pythagore 10
-- [(3,4,5),(6,8,10)]

