-- Définit une classe Encadrable qui dérive de la classe Show
-- et définit une fonction encadrer avec une implémentation
-- par défaut.
class (Show a) => Encadrable a where
  encadrer :: String -> a -> String
  encadrer s x = s ++ show x ++ s

-- Instancie Encadrable pour le type Double avec 
-- l'implémentation par défaut de la fonction encadrer.
instance Encadrable Double 

-- Instancie Encadrable pour le type Int avec une 
-- implémentation explicite de la fonction encadrer.
instance Encadrable Int where
  encadrer s x = s ++ " " ++ show x ++ " " ++ s

-- Définit un nouveau type avec instanciation explicite
-- de Show et instanciation par défaut de Encadrable.
data OuiNon = Oui | Non

instance Show OuiNon where
  show Oui = "oui"
  show Non = "non"

instance Encadrable OuiNon

main = do
  putStrLn $ encadrer "**" (42::Int)
  putStrLn $ encadrer "**" (42::Double)
  putStrLn $ encadrer "--" Oui

