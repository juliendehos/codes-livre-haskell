-- Multiple par 2 une valeur contenue dans un Maybe.
fois2 :: Maybe Double -> Maybe Double
fois2 Nothing = Nothing
fois2 (Just x) = Just (x*2)

-- Calcule la racine d'un réel positif ou retourne Nothing.
racine :: Double -> Maybe Double
racine x
  | x >= 0 = Just (sqrt x)
  | otherwise = Nothing

main = do
  print $ fois2 $ racine (-4)
  print $ fois2 $ racine 4
