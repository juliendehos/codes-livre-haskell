-- Définit un type paramétrique (Angle a) sans 
-- contrainte sur le paramètre de type.
data Angle a = Degre a | Radian a

-- Instancie Show pour (Angle a) sous la 
-- contrainte que a est de classe Show.
instance Show a => Show (Angle a) where
  show (Degre d) = show d ++ " deg"
  show (Radian r) = show r ++ " rad"

main = do
  print $ Degre (180::Float)
  print $ Radian (pi::Double)

