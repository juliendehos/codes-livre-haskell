-- Définit un type produit, représentant les couleurs RVB.
data Couleur = Rvb Double Double Double deriving Show

-- Retourne la couleur inverse (utilise un filtrage 
-- par motif pour récupérer les composantes).
inverser :: Couleur -> Couleur
inverser (Rvb r v b) = Rvb (1-r) (1-v) (1-b)

main = print $ inverser (Rvb 1 0.4 0)

