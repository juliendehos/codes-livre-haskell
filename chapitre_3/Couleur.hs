-- Nouveau type somme, dérivant des classes Eq et Show.
data Couleur = Blanc | Noir deriving (Eq, Show)

-- Utilise (==) sur le nouveau type (classe Eq).
inverser :: Couleur -> Couleur
inverser x = if x == Blanc then Noir else Blanc

-- Utilise show sur le nouveau type (classe Show).
main = putStrLn $ show $ inverser Blanc

