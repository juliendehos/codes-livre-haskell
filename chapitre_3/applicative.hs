data Personne = Personne 
  { nom :: String
  , age :: Int
  } deriving (Show)

validerNom :: String -> Maybe String
validerNom n = if null n then Nothing else Just n

validerAge :: Int -> Maybe Int
validerAge a = if a <= 0 then Nothing else Just a

-- Creer une personne valide en utilisant le "style applicatif".
creerPersonne :: String -> Int -> Maybe Personne
creerPersonne n a = Personne <$> validerNom n <*> validerAge a

main = do
  print $ creerPersonne "John" 42
  print $ creerPersonne "" 42

