
doubler :: Functor f => f Int -> f Int
doubler = fmap (*2)

main = do
    print $ fmap (*2) [1..3]
    print $ fmap (*2) (Just 21)
    print $ fmap (*2) (13, 37)

    print $ doubler [1..3]
    print $ doubler (Just 21)
    print $ doubler (13, 37)

    print $ 42 <$ [13, 37]
    print $ 42 <$ Just 37
    print $ 42 <$ (1, 2)
