-- Définit un synonyme de type.
type Point2d = (Double, Double)

-- Fonction définie avec le type synonyme.
symetrique :: Point2d -> Point2d
symetrique (x, y) = (-x, -y)

-- Fonction définie avec le type de base.
symetrique' :: (Double, Double) -> (Double, Double)
symetrique' (x, y) = (-x, -y)

-- Utiliser Point2d ou (Double, Double) est équivalent.
main = print $ symetrique' $ symetrique (13.0, 37.0) 

