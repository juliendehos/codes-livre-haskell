-- Définit un type Forme, de constructeurs Carre et Rect.
data Forme = Carre Double
           | Rect Double Double
           deriving Show

-- Filtrage par motif du type algébrique Forme.
surface :: Forme -> Double
surface (Carre cote) = cote^2
surface (Rect largeur hauteur) = largeur*hauteur

-- Évaluation partielle du constructeur Rect.
rectLargeur2 :: Double -> Forme
rectLargeur2 = Rect 2.0

main = print $ map surface formes
  where formes = [Carre 2.0, Rect 3.0 2.0, rectLargeur2 4.0]

