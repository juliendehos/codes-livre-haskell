-- Type algébrique à deux paramètres.
data Resultat a b = Erreur a | Valide b deriving Show

-- Utilisation du type pour définir une fonction.
racineCarree :: Double -> Resultat String Double
racineCarree x = if x < 0 
                 then Erreur "racine d'un nombre negatif"
                 else Valide $ sqrt x

main = do
  print $ racineCarree 16
  print $ racineCarree (-7)

