import Control.Monad (filterM, foldM)

evenIO :: Int -> IO Bool
evenIO = return . even

addIO :: Int -> Int -> IO Int
addIO x y = return $ x + y

main :: IO ()
main = do
  -- Filtrage avec une fonction monadique.
  filterM evenIO [1..3] >>= print

  -- Réduction avec une fonction monadique.
  foldM addIO 0 [1..3] >>= print

