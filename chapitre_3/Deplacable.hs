-- Point Disque Rect

data Point = Point {x::Double, y::Double} deriving Show
data Disque = Disque {centre::Point, rayon::Double} deriving Show
data Rect = Rect {p0::Point, p1::Point} deriving Show

plus :: Point -> Point -> Point
(Point x0 y0) `plus` (Point x1 y1) = Point (x0+x1) (y0+y1)

fois :: Double -> Point -> Point
k `fois` (Point x y) = Point (k*x) (k*y)

milieu :: Point -> Point -> Point
milieu p0 p1 = fois 0.5 $ p0 `plus` p1

-- Posable

class Posable a where
  poser :: a -> Point

instance Posable Disque where
  poser = centre

instance Posable Rect where
  poser (Rect p0 p1) = milieu p0 p1

-- Deplacable

class Posable a => Deplacable a where
  deplacer :: Point -> a -> a

instance Deplacable Disque where
  deplacer p (Disque c r) = Disque (c `plus` p) r

instance Deplacable Rect where
  deplacer p (Rect p0 p1) = Rect (p0 `plus` p) (p1 `plus` p)

-- Main

deplacerX1Y2 :: Deplacable a => a -> a
deplacerX1Y2 = deplacer (Point 1 2) 

afficher :: (Show a, Deplacable a) => a -> String
afficher x = show x ++ ", " ++ show (poser x)

main = do
  putStrLn $ "d  : " ++ afficher d
  putStrLn $ "d' : " ++ afficher d'
  putStrLn $ "r  : " ++ afficher r
  putStrLn $ "r' : " ++ afficher r'
    where d = Disque (Point 1 2) 4
          r = Rect (Point 3 3) (Point 6 4)
          d' = deplacerX1Y2 d
          r' = deplacerX1Y2 r

