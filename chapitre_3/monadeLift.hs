import Control.Monad (liftM)

-- Lit un Int dans un String et retourne son double.
readAndDouble :: String -> Int
readAndDouble = (*2) . read

main = do
  putStrLn "Entrez un entier"
  liftM readAndDouble getLine >>= print

