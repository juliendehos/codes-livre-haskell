import Control.Monad (replicateM)

-- Affiche un message et saisit un entier au clavier.
getInt :: IO Int
getInt = putStrLn "n ?" >> getLine >>= return . read

-- Applique la fonction précédente deux fois et affiche
-- les résultats collectés. 
main :: IO ()
main = replicateM 2 getInt >>= print 

