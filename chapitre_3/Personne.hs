-- Type d'enregistrement.
data Personne = Personne {
  nom::String, 
  prenom::String, 
  age::Int}

-- Utilisation des fonctions d'accès (prenom, nom, age).
info :: Personne -> String
info p = concat [prenom p, " ", nom p, " ", show $ age p]

-- Filtrage par motif classique.
nomPrenom :: Personne -> (String, String)
nomPrenom (Personne n p _) = (n, p)

main = do
  print $ info curry
  print $ nomPrenom church
    where curry = Personne "Curry" "Haskell" 118
          church = Personne {prenom="Alonzo",nom="Church",age=115}
