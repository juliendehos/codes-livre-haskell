module ModOuiNon 
  ( OuiNon(..)    -- Exporte le type OuiNon et ses constructeurs.
  , contredire
  ) where

data OuiNon = Oui | Non deriving Show

contredire :: OuiNon -> OuiNon
contredire Oui = Non
contredire Non = Oui

