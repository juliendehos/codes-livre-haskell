v1 :: Maybe String
v1 = return "hello"
-- Just "hello"

v2 :: Maybe String
v2 = Nothing
-- Nothing

reverseM :: String -> Maybe String
reverseM = return . reverse

v3 :: Maybe String
v3 = v1 >>= reverseM
-- Just "olleh"

v4 :: Maybe String
v4 = v2 >>= reverseM
-- Nothing

headM :: String -> Maybe Char
headM s = if null s then Nothing else return (head s)

v5 :: Maybe Char
v5 = v1 >>= reverseM >>= headM
-- Just 'o'

v6 :: Maybe Char
v6 = Just "" >>= reverseM >>= headM
-- Nothing

v7 :: Maybe Char
v7 = do
  x1 <- v1           -- Extrait "hello" de Just "hello".
  x2 <- reverseM x1  -- Applique reverseM et extrait le résultat.
  headM x2           -- Produit le résultat final de l'expression.
-- Just 'o'

v8 :: Maybe Char
v8 = do
  let x1 = ""        -- Définit une variable locale.
  x2 <- reverseM x1
  headM x2
-- Nothing

v9 :: Maybe String
v9 = do
  x1 <- v1
  reverseM x1  -- Produit une valeur non utilisée au final.
  v1           -- Résultat final de l'expression.
-- Just "hello"

v10 :: Maybe String
v10 = do
  x1 <- v1
  v1
  reverseM x1
-- Just "olleh"

v11 :: Maybe String
v11 = do
  x1 <- v1
  return ""    -- Définit une valeur monadique, non utilisée.
  reverseM x1  -- Résultat final de l'expression.
-- Just "olleh"

v12 :: Maybe String
v12 = do
  x1 <- v1        -- Produit la valeur x1.
  x2 <- headM x1  -- Produit la valeur x2.
  reverseM x1     -- Utilise x1 et non la valeur précédente x2.
-- Just "olleh"

v13 :: Maybe String  -- Avec la notation classique, il faut
v13 = v1             -- passer par une lambda intermédiaire.
      >>= \ x -> (headM x >> return x)
      >>= reverseM
-- Just "olleh"

main = do
  print v1
  print v2
  print v3
  print v4
  print v5
  print v6
  print v7
  print v8
  print v9
  print v10
  print v11
  print v12
  print v13

