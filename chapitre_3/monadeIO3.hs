-- Définit une action IO 
-- contenant un Int.
v1 :: IO Int
v1 = print "v1" >> return 21

-- Définit une valeur "pure".
v1' :: Int
v1' = 21

-- Définit une fonction retournant une action IO.
f1 :: Int -> IO Int
f1 x = do
  print "f1"
  return $ x*2

-- Définit une fonction "pure".
f1' :: Int -> Int
f1' x = x*2

main :: IO ()
main = do
  v1 >>= print        -- Affiche "v1" puis 21.
  print v1'           -- Affiche 21.
  v1 >>= f1 >>= print -- Affiche "v1" puis "f1" puis 42.
  print $ f1' v1'     -- Affiche 42.

