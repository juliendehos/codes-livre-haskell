-- Définit un type d'énumération pour les jours de la semaine.
data Jour = Lundi | Mardi | Mercredi | Jeudi 
          | Vendredi | Samedi | Dimanche

-- Fonction utilisant le type Jour.
estWeekend :: Jour -> Bool
estWeekend Samedi = True
estWeekend Dimanche = True
estWeekend _ = False

main = print $ map estWeekend [Lundi, Samedi, Jeudi]

