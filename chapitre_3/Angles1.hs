-- Définit un type Angle et demande l'instanciation par défaut 
-- de la classe Eq (opérateurs == et /=) pour ce type.
data Angle = Degre Double | Radian Double deriving Eq

-- Utilise l'opérateur == sur des valeurs de type Angle.
main = print $ Degre 180.0 == Radian pi

