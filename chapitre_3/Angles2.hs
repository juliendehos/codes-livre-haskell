-- Définit un type Angle.
data Angle = Degre Double 
           | Radian Double

-- Instancie la classe Eq pour le type Angle en gérant également 
-- la comparaison dans deux unités différentes. 
instance Eq Angle where
  (Degre d1) == (Degre d2) = d1 == d2
  (Radian r1) == (Radian r2) = r1 == r2
  a1 == a2 = a1 == convertir a2

-- Définit une fonction de conversion degré <-> radian.
convertir :: Angle -> Angle
convertir (Degre d) = Radian $ d*pi/180.0
convertir (Radian r) = Degre $ r*180.0/pi

main = print $ Degre 180.0 == Radian pi

