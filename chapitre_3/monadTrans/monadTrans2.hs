import Control.Monad (guard)
import Control.Monad.Trans (lift)
import Control.Monad.Trans.Maybe (MaybeT, runMaybeT)

getNameM :: MaybeT IO String
getNameM = do
  lift $ putStrLn "Name ?"
  name <- lift getLine
  guard (name /= "")  -- Retourne Nothing si name est vide.
  return name         -- Retourne un Just sinon.

main = runMaybeT getNameM >>= print

