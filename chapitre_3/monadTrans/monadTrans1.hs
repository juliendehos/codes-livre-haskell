getNameM :: IO (Maybe String)
getNameM = do
  putStrLn "Name ?"
  name <- getLine
  let maybeName = if null name then Nothing else Just name
  return maybeName

main = getNameM >>= print

