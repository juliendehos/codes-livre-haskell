## avec Stack

```
stack setup
stack build
stack exec monadTrans1
stack exec monadTrans2
```

## avec Nix

```
nix-shell --run "cabal run monadTrans1"
nix-shell --run "cabal run monadTrans2"
```


