import Control.Applicative (liftA)

-- Transforme la fonction (*2) de Double -> Double en 
-- Maybe Double -> Maybe Double.
fois2 :: Maybe Double -> Maybe Double
fois2 = liftA (*2)

racine :: Double -> Maybe Double
racine x
  | x >= 0 = Just (sqrt x)
  | otherwise = Nothing

main = do
  print $ fois2 $ racine (-4)
  print $ fois2 $ racine 4
