import Contacts
import Control.Monad (MonadPlus, mzero)

testerNomM :: MonadPlus m => Contact -> m String 
testerNomM (Contact n _) = if n == "" then mzero else return n

main = do
  print $ bd1 >>= testerNomM
  print $ mb1 >>= testerNomM
  print $ mb2 >>= testerNomM

