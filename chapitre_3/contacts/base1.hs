import Contacts

chercher :: String -> [Contact] -> Bool
chercher n b = elem n (map nom_ b)

inserer :: Contact -> [Contact] -> [Contact]
inserer contact base = if found then base else contact:base
  where found = chercher (nom_ contact) base

supprimer :: String -> [Contact] -> [Contact]
supprimer nom = filter ((/=nom) . nom_)

main :: IO ()
main = do
  -- Cherche John dans bd1.
  print $ chercher "John" bd1
  -- Insère Oscar et enlève John de bd1.
  let bd2 = inserer (Contact "Oscar" "oscar@example.com") bd1
      bd3 = supprimer "John" bd2
  print bd3

