import Contacts

testerNomListe :: Contact -> [String] 
testerNomListe (Contact n _) = if n == "" then [] else [n]

testerNomMaybe :: Contact -> Maybe String 
testerNomMaybe (Contact n _) = if n == "" then Nothing else Just n

main = do
  print $ bd1 >>= testerNomListe
  print $ mb1 >>= testerNomMaybe
  print $ mb2 >>= testerNomMaybe

