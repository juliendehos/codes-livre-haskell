module Contacts where

data Contact = Contact 
  { nom_   :: String
  , email_ :: String
  } deriving Show

-- Exemple de liste de Contact.
bd1 :: [Contact]
bd1 = 
  [ Contact "John" "john@example.com"
  , Contact "" "inconnu@example.com" ]

-- Exemple de Maybe Contact.
mb1 :: Maybe Contact
mb1 = Just $ Contact "John" "John@example.com"

-- Exemple de Maybe Contact.
mb2 :: Maybe Contact
mb2 = Just $ Contact "" "inconnu@example.com"

