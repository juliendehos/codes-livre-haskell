import Contacts

chercherM :: Monad m => String -> [Contact] -> m Bool
chercherM n b = return $ elem n (map nom_ b)

insererM :: Monad m => Contact -> [Contact] -> m [Contact]
insererM contact base = do
  found <- chercherM (nom_ contact) base
  return $ if found then base else (contact:base)

supprimerM :: Monad m => String -> [Contact] -> m [Contact]
supprimerM nom = return . filter ((/=nom) . nom_)

main :: IO ()
main = do
  -- Cherche John dans bd1.
  chercherM "John" bd1 >>= print
  -- Insère Oscar et enlève John de bd1.
  insererM (Contact "Oscar" "oscar@example.com") bd1
    >>= supprimerM "John" 
    >>= print 

  -- Suite du main.

  -- Cherche John dans bd1.
  foundJohn <- chercherM "John" bd1
  print foundJohn
  -- Insère Oscar et enlève John de bd1.
  bd2 <- insererM (Contact "Oscar" "oscar@example.com") bd1
  bd3 <- supprimerM "John" bd2
  print bd3

  -- Suite du main.

  -- Utilisation dans une monade Maybe.
  print $ Just bd1 >>= chercherM "John"
  print $ Just bd1 
            >>= insererM (Contact "Oscar" "oscar@example.com") 
            >>= supprimerM "John" 

  -- Utilisation dans une monade Maybe, avec la notation "do".
  print $ do
    bd1' <- Just bd1
    bd2' <- insererM (Contact "Oscar" "oscar@example.com") bd1'
    supprimerM "John" bd2'

