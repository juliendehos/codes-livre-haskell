import Contacts

testerNomListe :: [Contact] -> [String] 
testerNomListe = filter (/="") . map (\(Contact n _) -> n)

testerNomMaybe :: Maybe Contact -> Maybe String 
testerNomMaybe Nothing = Nothing
testerNomMaybe (Just (Contact n _)) = 
  if n == "" then Nothing else Just n

main = do
  print $ testerNomListe bd1
  print $ testerNomMaybe mb1
  print $ testerNomMaybe mb2

