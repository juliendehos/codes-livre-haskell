import Data.Char (toUpper)

main :: IO ()  -- Main est une action IO.
main = do
  putStrLn "Entrez un texte :"  -- Affiche à l'écran.
  txt <- getLine                -- Saisit au clavier.
  let txt' = map toUpper txt    -- Transforme la saisie.
  putStrLn txt'                 -- Affiche le résultat.

