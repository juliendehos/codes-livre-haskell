import Control.Monad
import Control.Monad.State.Lazy

-- Calcule les termes successifs de la suite de Fibonacci.
fibo :: State (Int, Int) Int
fibo = do 
  (fnm2, fnm1) <- get
  put (fnm1, fnm1+fnm2) 
  return fnm2

-- Calcule 3 étapes successives de la suite.
fiboTroisFois :: State (Int, Int) Int
fiboTroisFois = do 
  f0 <- fibo
  f1 <- fibo
  f2 <- fibo
  return f2

-- État initial (f0 = 0 et f1 = 1).
s0 :: (Int, Int)
s0 = (0, 1)

main :: IO ()
main = do
  -- Calcule les 10 premiers termes.
  print $ evalState (replicateM 10 fibo) s0

  -- Calcule le 3e terme (f2).
  print $ evalState fiboTroisFois s0

