import Control.Monad.State.Lazy

compteur :: State Int Int
compteur = do 
  n <- get
  put (n+1)
  return n

main = do
  print $ runState compteur 1                  -- (1,2)
  print $ evalState compteur 1                 -- 1
  print $ execState compteur 1                 -- 2
  print $ runState (replicateM 2 compteur) 1   -- ([1,2],3)
  print $ evalState (replicateM 2 compteur) 1  -- [1,2]
  print $ execState (replicateM 2 compteur) 1  -- 3

