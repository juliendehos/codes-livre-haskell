## avec Stack

```
stack setup
stack build
stack exec fibo
...
```

## avec Nix

```
nix-shell --run "cabal run fibo"
...
```

ou

```
nix-build
./result/bin/fibo
...
```

