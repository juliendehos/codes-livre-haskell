module State where

data State s a = State { runState :: s -> (a, s) }

instance Functor (State s) where
  fmap f m  = State $ \s -> let (r, s') = runState m s 
                            in (f r, s')

instance Applicative (State s) where
  pure x   = State $ \s -> (x, s)

  m <*> n  = State $ \s -> let (f, s')  = runState m s 
                               (x, s'') = runState n s'
                           in (f x, s'')

instance Monad (State s) where
  m >>= n  = State $ \s -> let (r, s') = runState m s 
                           in runState (n r) s'

get :: State s s
get = State $ \s -> (s, s)

put :: s -> State s ()
put s' = State $ \_ -> ((), s')

evalState :: State s a -> s -> a
evalState s i = fst $ runState s i

execState :: State s a -> s -> s
execState s i = snd $ runState s i

