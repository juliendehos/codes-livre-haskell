{ pkgs ? import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/18.03.tar.gz") {} }:
let 
  drv = pkgs.haskellPackages.callCabal2nix "stateMonad" ./. {};
in
if pkgs.lib.inNixShell then drv.env else drv

