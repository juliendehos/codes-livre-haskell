import Control.Monad (replicateM)
import State (State, put, get, evalState, runState)
import System.Random (getStdGen, StdGen, randomR)

genVoisin :: State (Int, StdGen) Int
genVoisin = do
  (x0, gen0) <- get
  let xs = [x | x<-[x0-1 .. x0+1]]
      (i1, gen1) = randomR (0, (length xs) - 1) gen0
      x1 = xs !! i1
  put (x1, gen1)
  return x1

repeter :: Int -> State s a -> s -> [a]
repeter 0 _ _ = []
repeter n m s = 
  let (r', s') = runState m s
  in r' : repeter (n-1) m s'

main :: IO ()
main = do
  gen0 <- getStdGen
  let s0 = (0, gen0)
  print $ repeter 5 genVoisin s0
  print $ evalState (replicateM 5 genVoisin) s0

