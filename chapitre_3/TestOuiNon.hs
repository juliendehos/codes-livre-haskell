-- Importe le type OuiNon et le constructeur Oui.
import ModOuiNon (OuiNon(Oui), contredire)

main = do
  print Oui
  print $ contredire Oui

