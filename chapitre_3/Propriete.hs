-- Définit un type algébrique paramétrique "Propriete a".
data Propriete a = Propriete String a 

-- Définit une variable de type "Propriete Int".
prop1 :: Propriete Int
prop1 = Propriete "age" 12

-- Définit une variable de type "Propriete String".
prop2 :: Propriete String
prop2 = Propriete "couleur" "bleu"

-- Fonction prenant un paramètre de type "Propriete a".
formaterProp :: Show a => Propriete a -> String
formaterProp (Propriete k v) = k ++ ": " ++ show v

main = do
  putStrLn $ formaterProp prop1
  putStrLn $ formaterProp prop2

