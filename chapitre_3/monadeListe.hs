fois2M :: Int -> [Int]
fois2M x = [x*2]

plus1M :: Int -> [Int]
plus1M x = return $ x+1

liste1 :: [Int]
liste1 = [1..4] >>= fois2M >>= plus1M
-- [3,5,7,9]

liste2 :: [Int]
liste2 = return 1 >>= fois2M >>= plus1M
-- [3]

liste3 :: [Int]
liste3 = do
  x <- [1..2]
  fois2M x
-- [2,4]

liste4 :: [Int]
liste4 = do
  x <- [1..2]
  y <- [1..2]
  return $ x+y
-- [2,3,3,4]

main = do
  print liste1
  print liste2
  print liste3
  print liste4

