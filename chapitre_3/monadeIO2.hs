import Data.Char (toUpper)

main :: IO ()
main = putStrLn "Entrez un texte :"
   >>  getLine
   >>= putStrLn . map toUpper

