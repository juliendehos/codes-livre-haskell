estPositif :: Functor f => f Int -> f Bool
estPositif = fmap (>= 0)

main = do
  print $ estPositif (Just 21)
  print $ estPositif [-1, 21]

