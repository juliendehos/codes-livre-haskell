-- Racine d'un réel,
-- avec message d'erreur si le réel est négatif.
racine :: Double -> Either String Double
racine x
  | x < 0 = Left "racine d'un nombre negatif"
  | otherwise = Right (sqrt x)

main = do
  print $ racine (-4)
  print $ racine 4

