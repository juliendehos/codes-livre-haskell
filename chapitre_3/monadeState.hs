data State s a = State { runState :: s -> (a, s) }

-----------------------------------------------------------

m1 :: State Int Int
m1 = State $ \s -> (s, s+1)

t1 = runState m1 0
-- (0, 1)

-----------------------------------------------------------

m2 :: State String Char
m2 = State $ \(x:xs) -> (x, xs)

t2 = runState m2 "hello"
-- ('h', "ello")

t2' = let (r1, s1) = runState m2 "hello"
      in runState m2 s1
-- ('e', "llo")

t2'' = let (r1, s1) = runState m2 "hello"
           (r2, s2) = runState m2 s1
       in ([r1,r2], s2)
-- ("he", "llo")

-----------------------------------------------------------

evalState :: State s a -> s -> a
evalState m s = fst $ runState m s

execState :: State s a -> s -> s
execState m s = snd $ runState m s

r2 = evalState m2 "hello"
-- 'h'

s2 = execState m2 "hello"
-- "ello"

-----------------------------------------------------------

{-
instance Monad (State s) where

  return :: s -> State s s
  return r = State $ \s -> (r, s)

  (>>=) :: State s a -> (a -> State s b) -> State s b
  m >>= n = State $ \s -> let (r, s') = runState m s
                          in runState (n r) s'

-- Attention : ce code ne compile pas directement.
-}

m3 :: State String Char
m3 = return 'a'  -- Encapsule la valeur 'a' dans la monade.

t3 = runState m3 "hello"
-- ('a', "hello")

-----------------------------------------------------------

f4 :: Char -> State String String
f4 x = State $ \(y:ys) -> ([x, y], ys)

m4 :: State String String
m4 = m3 >>= f4

t4 = runState m4 "hello"
-- ("ah", "ello")

m5 :: State String String
m5 = do    -- Autre implémentation possible de m4.
  t <- m3
  f4 t

t5 = runState m5 "hello"
-- ("ah", "ello")

-----------------------------------------------------------

put :: s -> State s ()
put s' = State $ \_ -> ((), s')

get :: State s s
get = State $ \s -> (s, s)

-----------------------------------------------------------

m6 :: State Int Int
m6 = do        -- Autre implémentation possible de m1 :
  x <- get     -- récupère l'état courant,
  put $ x + 1  -- place le nouvel état,
  return x     -- encapsule la valeur dans la monade.

t6 = runState m6 0
-- (0, 1)

m7 :: State String Char
m7 = do          -- Autre implémentation possible de m2.
  (x:xs) <- get
  put xs
  return x

t7 = runState m7 "hello"
-- ('h', "ello")

-----------------------------------------------------------

main = do
  print t1
  print t2
  print t2'
  print t2''
  print r2
  print s2
  print t3
  print t4
  print t5
  print t6
  print t7

-----------------------------------------------------------

instance Functor (State s) where
  fmap f m  = State $ \s -> let (r, s') = runState m s 
                            in (f r, s')

instance Applicative (State s) where
  pure x   = State $ \s -> (x, s)

  m <*> n  = State $ \s -> let (f, s')  = runState m s 
                               (x, s'') = runState n s'
                           in (f x, s'')

instance Monad (State s) where
  m >>= n  = State $ \s -> let (r, s') = runState m s 
                           in runState (n r) s'

