-- Arbre binaire de recherche
-- (type algébrique paramétrique récursif).
data Abr a = Feuille | Noeud a (Abr a) (Abr a)

-- Cherche une valeur dans un ABR.
chercher :: Ord a => a -> Abr a -> Bool
chercher _ Feuille = False
chercher x (Noeud y ag ad)
  | x < y = chercher x ag
  | x > y = chercher x ad
  | otherwise = True

-- Insère une valeur dans un ABR.
inserer :: Ord a => a -> Abr a -> Abr a
inserer x Feuille = Noeud x Feuille Feuille
inserer x n@(Noeud y ag ad) 
  | x < y = Noeud y (inserer x ag) ad
  | x > y = Noeud y ag (inserer x ad)
  | otherwise = n

-- Convertit un ABR en liste.
abrVersList :: Abr a -> [a]
abrVersList Feuille = []
abrVersList (Noeud x ag ad) = lg ++ [x] ++ ld
  where lg = abrVersList ag
        ld = abrVersList ad

-- Convertit une liste en ABR.
listVersAbr :: Ord a => [a] -> Abr a
listVersAbr = foldr inserer Feuille 

main = do
  print $ chercher 13 abr
  print $ chercher 14 abr
  print $ abrVersList abr 
    where abr = listVersAbr [37,13,42,13,15]

