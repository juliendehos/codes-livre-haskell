-- Définit une classe Mesurable contenant des 
-- fonctions surface et perimetre.
class Mesurable a where
  surface :: a -> Double
  perimetre :: a -> Double

-- Crée un type Forme qui instancie la classe Mesurable.
data Forme = Carre Double | Rect Double Double 
instance Mesurable Forme where
  surface (Carre cote) = cote^2
  surface (Rect largeur hauteur) = largeur*hauteur
  perimetre (Carre cote) = 4*cote
  perimetre (Rect largeur hauteur) = 2*(largeur+hauteur)

-- Crée un type Terrain qui instancie la classe Mesurable.
data Terrain = Stade | Champ Double Double
instance Mesurable Terrain where
  surface Stade = 7266 
  surface (Champ s _) = s
  perimetre Stade = 360 
  perimetre (Champ _ p) = p

-- Définit une fonction sur des types de classe Mesurable.
afficherMesures :: Mesurable a => a -> String
afficherMesures x = "s = " ++ show (surface x) 
                 ++ ", p = " ++ show (perimetre x)

main = do
  putStrLn $ afficherMesures (Rect 2 3)
  putStrLn $ afficherMesures Stade

