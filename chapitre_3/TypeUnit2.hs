-- Réalise des entrées/sorties sans produire de valeur. 
saisirMots :: IO ()
saisirMots = do
  str <- getLine     -- Saisit une ligne dans str.
  print $ words str  -- Découpe str en une liste de mots.
  return ()          -- Retourne la valeur Unit.

main :: IO ()
main = do
  putStrLn "Saisir une ligne de mots:"
  saisirMots

