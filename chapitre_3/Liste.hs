-- Implémente une liste d'entiers.
data Liste = Nil | Cons Int Liste

-- Convertit une Liste en [Int].
listeVersList :: Liste -> [Int]
listeVersList Nil = []
listeVersList (Cons x xs) = x : listeVersList xs

-- Convertit un [Int] en Liste.
listVersListe :: [Int] -> Liste
listVersListe = foldr Cons Nil  -- Réduction depuis la droite.

main = do
  print $ listeVersList $ Cons 13 (Cons 37 Nil)
  print $ listeVersList $ listVersListe [1..4]

