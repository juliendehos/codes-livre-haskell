isRep :: Eq a => [a] -> Bool
isRep (x0:x1:xs) = if x0 /= x1 then False else isRep (x1:xs)
isRep _ = True

main = do
  print $ isRep [1,1,1]
  print $ isRep "aab"

