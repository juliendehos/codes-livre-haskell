-- Définit une liste en utilisant les constructeurs.
liste1234 :: [Int]
liste1234 = 1 : 2 : 3 : 4 : []

-- Filtrage des constructeurs.
headZero :: [Int] -> Int
headZero [] = 0
headZero (x:_) = x

main = do
  print $ headZero liste1234
  print $ headZero []

