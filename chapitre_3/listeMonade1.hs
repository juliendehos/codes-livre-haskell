-- Exemple de fonction monadique.
doublerIO :: Int -> IO Int
doublerIO = return . (*2)

main :: IO ()
main = do
  -- Applique une fonction monadique sur chaque élément
  -- d'une liste et affiche la liste résultante.
  mapM doublerIO [1..3] >>= print

  -- Applique "print" sur chaque élément d'une liste.
  mapM_ print [1..3]

