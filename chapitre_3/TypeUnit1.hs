-- Remplace les éléments d'une liste par des valeurs ().
simplifierListe :: [a] -> [()]
simplifierListe = map (\ _ -> ())

main = print $ simplifierListe [1..4]

