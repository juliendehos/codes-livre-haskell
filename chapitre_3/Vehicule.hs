data Vehicule = Velo {marque::String, taillePouces::Double}
              | Auto {marque::String, nbPlaces::Int}
              deriving Show

info :: Vehicule -> String
info (Velo m t) = "velo " ++ m ++ " " ++ (show t) ++ " pouces" 
info (Auto m n) = "auto " ++ m ++ " " ++ (show n) ++ " places"

main = do
  print $ map info [Auto "Talbot" 4, 
                    Velo {taillePouces=20.5, marque="United"}]

